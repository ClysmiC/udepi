# Udepi

## A spherical game prototype... written from scratch!

This is a very basic prototype. It was mostly written as an excercise to become more familiar with game engine programming. This prototype includes (and taught me a lot about):

* Rendering 3D meshes in OpenGL with shaders and lighting
* Rendering 2D text (using stb_truetype)
* GJK and EPA implementations for hit-detection and resolution
* Quaternions!
* Non-Euclidean (spherical) geometry!

[Check out](/readme_res/udepi.mp4)

[some clips](/readme_res/udepi.webm)