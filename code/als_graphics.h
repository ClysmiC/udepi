struct GraphicsShape
{
	GLuint vao;
	uint32 numTriangles;
};

struct CommonGraphicsShapes
{
	// 3D shapes
	GraphicsShape sphere;
	GraphicsShape cube;
	GraphicsShape cylinder;

	// 2D shapes
	GraphicsShape circle;
};

union GfxTriangle
{
	// NOTE: Vertices must wind CCW!
	struct
	{
		Vec3 a;
		Vec3 b;
		Vec3 c;

		Vec3 aNormal;
		Vec3 bNormal;
		Vec3 cNormal;
	};
	struct
	{
		Vec3 points[3];
		Vec3 normals[3];
	};
};

struct Light
{
	Vec3 position;

	// Note: coefficients for r, g, b, with values 0-1
	Vec3 ambient;
	Vec3 diffuse;
	Vec3 specular;
};

struct Material
{
	Vec3 ambient;
	Vec3 diffuse;
	Vec3 specular;
	real32 shininess;
};

struct Font
{
	uint32 glTexture;
	uint32 glTextureDimension;

	// I believe "height" in the stb definiton of the word is the
	// range from the lowest descender to the highest ascender.
	// I care more about the height from the baseline to the
	// highest ascender, so after baking a font I will just
	// calculate the height of the letter "A" and store it here
	real32 capsHeight;

	real32 lineHeight;
	
	stbtt_bakedchar characterData[96];
};

struct Scene
{
	Camera* camera;
	CommonGraphicsShapes commonShapes;

	Font* font;
	
	// TODO: replace these with custom mat4 object
	Mat4 view;         // Maps world space to camera space
	Mat4 projection;   // Adds projective perspection

	uint32 lightCount;
	
	uint32 maxLightCount = 16;
	Light lights[16] = {0};

	~Scene()
	{
		if(font)
		{
			free(font);
		}

		// What else do here?
	}
};
