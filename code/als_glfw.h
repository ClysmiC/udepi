void glfwErrorCallback(int e, const char* message)
{
	printf("%s\n", message);
}
	
void glfwKeyPressed(GLFWwindow* window, int key, int scanCode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (action == GLFW_PRESS)
	{
		keys[key] = true;
	}

	if (action == GLFW_RELEASE)
	{
		keys[key] = false;
	}
}

void glfwWindowResizeCallback(GLFWwindow* window, int width, int height)
{
	glfwGetFramebufferSize	(window, &width, &height);
	
	glViewport(0, 0, width, height);

	// TODO: recalculate the perspective matrix!
}

int initWindow(GLFWwindow** window, uint32 width, uint32 height)
{
	// Initialize GLFW, create and open window with OpenGL context
	if (!glfwInit())
	{
		printf("Failed to initialize GLFW. Exiting.\n");
		return -1;
	}

	glfwSetErrorCallback(&glfwErrorCallback);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	
	*window = glfwCreateWindow(width, height, "Game", NULL, NULL);
	glfwMakeContextCurrent(*window);

	glViewport(0, 0, width, height);
	
	glfwSetKeyCallback(*window, &glfwKeyPressed);
	glfwSetWindowSizeCallback(*window, &glfwWindowResizeCallback);
	
	glewExperimental = GL_TRUE;
	GLenum glewInitStatus = glewInit();

	// GLEW
	if(glewInitStatus != GLEW_OK)
	{
		printf("Failed to initialize GLEW. Exiting.\n");
		return -1;
	}

	// OpenGL setup
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Not sure how to reasonably implement proper diffuse shading for 2-sided
	// polygon... for now I will just cull back faces and if I need polygon to
	// be 2-sided, I'll draw two polygons with different winding orders.
	glEnable(GL_CULL_FACE);

	return 0;
}

//
// TODO: This is really OpenGL, not glfw specific. Find a better place for it.
//
GLuint compileShader(const GLchar *vPath, const GLchar *fPath)
{
	// File streams
	std::ifstream vStream;
	std::ifstream fStream;

	// String streams
	std::stringstream vSrc;
	std::stringstream fSrc;

	// Resulting strings
	std::string vString;
	std::string fString;

	vStream.open(vPath);
	vSrc << vStream.rdbuf();
	vStream.close();
	vString = vSrc.str();

	fStream.open(fPath);
	fSrc << fStream.rdbuf();
	fStream.close();
	fString = fSrc.str();

	// C-Strings
	const GLchar *vCode = vString.c_str();
	const GLchar *fCode = fString.c_str();

	GLuint vHandle, fHandle;
	GLint success;

	vHandle = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vHandle, 1, &vCode, NULL);
	glCompileShader(vHandle);

	glGetShaderiv(vHandle, GL_COMPILE_STATUS, &success);

	if(!success)
	{
		GLchar error[512];
		glGetShaderInfoLog(vHandle, 512, NULL, error);
		printf("ERROR::SHADER::VERTEX::COMPLIATION_FAILED\n%s", error);
	}

	fHandle = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fHandle, 1, &fCode, NULL);
	glCompileShader(fHandle);

	glGetShaderiv(fHandle, GL_COMPILE_STATUS, &success);

	if(!success)
	{
		GLchar error[512];
		glGetShaderInfoLog(fHandle, 512, NULL, error);
		printf("ERROR::SHADER::FRAGMENT::COMPLIATION_FAILED\n%s", error);
		return 0;
	}

	GLuint id = glCreateProgram();
	glAttachShader(id, vHandle);
	glAttachShader(id, fHandle);
	glLinkProgram(id);

	glGetProgramiv(id, GL_LINK_STATUS, &success);

	if(!success)
	{
		GLchar error[512];
		glGetProgramInfoLog(id, 512, NULL, error);
		printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s", error);
		return 0;
	}

	glDeleteShader(vHandle);
	glDeleteShader(fHandle);

	return id;
}
