union Vec3;
Vec3 vector3(real32 x, real32 y, real32 z);
bool32 isCollinear(Vec3 pointA, Vec3 pointB, Vec3 pointC);
Vec3 operator - (Vec3 vectorA, Vec3 vectorB);
Vec3 operator + (Vec3 vectorA, Vec3 vectorB);
Vec3 cross(Vec3 vectorA, Vec3 vectorB);
Vec3 normalize(Vec3 v);
bool32 isNormal(Vec3);
bool32 equals(Vec3 vectorA, Vec3 vectorB);

union Vec2
{
    struct
    {
        real32 x, y;
    };
    real32 element[2];
};

union Vec3
{
    struct
    {
        real32 x, y, z;
    };
    struct
    {
        real32 r, g, b;
    };
    real32 element[3];
};

union Vec4
{
    struct
    {
        real32 x, y, z, w;
    };
    struct
    {
        real32 r, g, b, a;
    };
    real32 element[4];
};

struct Mat3
{
	real32 _e[3][3] = {
		{ 1, 0, 0},
		{ 0, 1, 0},
		{ 0, 0, 1}
	};

	// Only overloads the first index to point to the correct array, then
	// the second index is handled by built-in c++
	real32* operator [] (int32 i)
	{
		real32* result = _e[i];
		return result;
	}

	real32* dataPointer()
	{
		real32* result = _e[0];
		return result;
	}
};

struct Mat4
{
	real32 _e[4][4] = {
		{ 1, 0, 0, 0},
		{ 0, 1, 0, 0},
		{ 0, 0, 1, 0},
		{ 0, 0, 0, 1}
	};

	// Only overloads the first index to point to the correct array, then
	// the second index is handled by built-in c++
	real32* operator [] (int32 i)
	{
		real32* result = _e[i];
		return result;
	}

	real32* dataPointer()
	{
		real32* result = _e[0];
		return result;
	}
};

typedef Vec4 Quaternion;

union Triangle
{
	struct
	{
		Vec3 a;
		Vec3 b;
		Vec3 c;
	};
	Vec3 elements[3];	

	Vec3 normal()
	{
		Vec3 ab = b - a;
		Vec3 ac = c - a;
		Vec3 n = cross(ab, ac);

		if(n.x == 0 && n.y == 0 && n.z == 0)
		{
			// This triangle is literally a single point...
			if(equals(a, b) && equals(a, c))
			{
				ALS_ILLEGAL;
				n = vector3(1, 0, 0);
			}
			
			// Isn't a true triangle, since ab and ac are parallel...
			// Simply return a vector that is parallel to bc
			if(equals(b, c))
			{
				// AB and AC are same
				Vec3 nonParallel = ab + vector3(1, 0, 0);

				n = cross(ab, nonParallel);

				ALS_ASSERT(n.x != 0 || n.y != 0 || n.z != 0);
			}
			else
			{
				// AB and AC are in opposite directions
				Vec3 bc = c - b;
				Vec3 nonParallel = bc + vector3(1, 0, 0);

				n = cross(bc, nonParallel);

				ALS_ASSERT(n.x != 0 || n.y != 0 || n.z != 0);
			}
		}
		
		n = normalize(n);

		ALS_ASSERT(isNormal(n));
		
		return n;
	}

	void rewind()
	{
		Vec3 temp;
		ALS_SWAP(&b, &c, Vec3);
	}
};

struct Edge3
{
	Vec3 p0;
	Vec3 p1;
};

Edge3 edge(Vec3 p0, Vec3 p1)
{
	Edge3 result;
	result.p0 = p0;
	result.p1 = p1;

	return result;
}

Triangle triangle(Vec3 a, Vec3 b, Vec3 c)
{
	Triangle result;
	result.a = a;
	result.b = b;
	result.c = c;

	return result;
}


struct Plane
{
	Vec3 point;
	Vec3 normal;
};

Plane plane(Vec3 point, Vec3 normal)
{
	Plane result;

	result.point = point;
	result.normal = normalize(normal);

	return result;
}

// NOTE: normal calculated using CCW winding
Plane plane(Vec3 pointA, Vec3 pointB, Vec3 pointC)
{
	ALS_ASSERT(!isCollinear(pointA, pointB, pointC));

	Vec3 ab = pointB - pointA;
	Vec3 ac = pointC - pointA;
	
	Plane result;
	result.point = pointA;
	result.normal = normalize(cross(ab, ac));

	return result;
}

Vec2 vector2(real32 value)
{
	Vec2 result;

    result.x = value;
    result.y = value;

    return result;
}

Vec2 vector2(real32 x, real32 y)
{
    Vec2 result;

    result.x = x;
    result.y = y;

    return result;
}

Vec3 vector3(real32 value)
{
	Vec3 result;

    result.x = value;
    result.y = value;
    result.z = value;

    return result;
}

Vec3 vector3(real32 x, real32 y, real32 z)
{
    Vec3 result;

    result.x = x;
    result.y = y;
    result.z = z;

    return result;
}

Vec3 vector3(Vec2 v, real32 z)
{
	Vec3 result = vector3(v.x, v.y, z);

	return result;
}

Vec4 vector4(real32 x, real32 y, real32 z, real32 w)
{
    Vec4 result;

    result.x = x;
    result.y = y;
    result.z = z;
    result.w = w;

    return result;
}

Vec4 vector4(real32 value)
{
	Vec4 result;

    result.x = value;
    result.y = value;
    result.z = value;
	result.w = value;

    return result;
}

Vec4 vector4(Vec2 v, real32 z, real32 w)
{
	Vec4 result = vector4(v.x, v.y, z, w);
	
	return result;
}

Vec4 vector4(Vec2 xy, Vec2 zw)
{
	Vec4 result = vector4(xy.x, xy.y, zw.x, zw.y);
	
	return result;
}

Vec4 vector4(Vec3 v, real32 w)
{
	Vec4 result = vector4(v.x, v.y, v.z, w);
	
	return result;
}

//
// OVERLOADED OPERATORS
//

// Vec2
Vec2 operator + (Vec2 vectorA, Vec2 vectorB)
{
	Vec2 result;

	result.x = vectorA.x + vectorB.x;
	result.y = vectorA.y + vectorB.y;
	
	return result;
}
Vec2 operator + (Vec2 vector, real32 constant)
{
	Vec2 result;

	result.x = vector.x + constant;
	result.y = vector.y + constant;
	
	return result;
}
Vec2& operator += (Vec2& vectorA, Vec2 vectorB)
{
	vectorA.x += vectorB.x;
	vectorA.y += vectorB.y;

	return vectorA;
}
Vec2& operator += (Vec2& vector, real32 constant)
{
	vector = vector + constant;
	return vector;
}

Vec2 operator - (Vec2 vectorA, Vec2 vectorB)
{
	Vec2 result;

	result.x = vectorA.x - vectorB.x;
	
	return result;
}
Vec2 operator - (Vec2 vector, real32 constant)
{
	Vec2 result;

	result.x = vector.x - constant;
	result.y = vector.y - constant;
	
	return result;
}
Vec2& operator -= (Vec2& vectorA, Vec2 vectorB)
{
	vectorA.x -= vectorB.x;
	vectorA.y -= vectorB.y;

	return vectorA;
}
Vec2& operator -= (Vec2& vector, real32 constant)
{
	vector = vector - constant;
	return vector;
}
Vec2 operator - (Vec2 vector)
{
	Vec2 result;
	result.x = -vector.x;
	result.y = -vector.y;

	return result;
}


Vec2 operator * (real32 scalar, Vec2 vector)
{
	Vec2 result;

	result.x = scalar * vector.x;
	result.y = scalar * vector.y;

	return result;
}
Vec2 operator * (Vec2 vector, real32 scalar)
{
	return scalar * vector;
}
Vec2& operator *= (Vec2& vector, real32 scalar)
{
	vector = vector * scalar;
	return vector;
}

Vec2 operator / (Vec2 vector, real32 scalar)
{
	Vec2 result;

	result.x = vector.x / scalar;
	result.y = vector.y / scalar;

	return result;
}
Vec2& operator /= (Vec2& vector, real32 scalar)
{
	vector = vector / scalar;
	return vector;
}


// Vec3
Vec3 operator + (Vec3 vectorA, Vec3 vectorB)
{
	Vec3 result;

	result.x = vectorA.x + vectorB.x;
	result.y = vectorA.y + vectorB.y;
	result.z = vectorA.z + vectorB.z;
	
	return result;
}
Vec3 operator + (Vec3 vector, real32 constant)
{
	Vec3 result;

	result.x = vector.x + constant;
	result.y = vector.y + constant;
	result.z = vector.z + constant;
	
	return result;
}
Vec3& operator += (Vec3& vectorA, Vec3 vectorB)
{
	vectorA.x += vectorB.x;
	vectorA.y += vectorB.y;
	vectorA.z += vectorB.z;	

	return vectorA;
}
Vec3& operator += (Vec3& vector, real32 constant)
{
	vector = vector + constant;
	return vector;
}

Vec3 operator - (Vec3 vectorA, Vec3 vectorB)
{
	Vec3 result;

	result.x = vectorA.x - vectorB.x;
	result.y = vectorA.y - vectorB.y;
	result.z = vectorA.z - vectorB.z;
	
	return result;
}
Vec3 operator - (Vec3 vector, real32 constant)
{
	Vec3 result;

	result.x = vector.x - constant;
	result.y = vector.y - constant;
	result.z = vector.z - constant;
	
	return result;
}
Vec3& operator -= (Vec3& vectorA, Vec3 vectorB)
{
	vectorA.x -= vectorB.x;
	vectorA.y -= vectorB.y;
	vectorA.z -= vectorB.z;

	return vectorA;
}
Vec3& operator -= (Vec3& vector, real32 constant)
{
	vector = vector - constant;
	return vector;
}
Vec3 operator - (Vec3 vector)
{
	Vec3 result;
	result.x = -vector.x;
	result.y = -vector.y;
	result.z = -vector.z;

	return result;
}


Vec3 operator * (real32 scalar, Vec3 vector)
{
	Vec3 result;

	result.x = scalar * vector.x;
	result.y = scalar * vector.y;
	result.z = scalar * vector.z;

	return result;
}
Vec3 operator * (Vec3 vector, real32 scalar)
{
	return scalar * vector;
}
Vec3& operator *= (Vec3& vector, real32 scalar)
{
	vector = vector * scalar;
	return vector;
}

Vec3 operator / (Vec3 vector, real32 scalar)
{
	Vec3 result;

	result.x = vector.x / scalar;
	result.y = vector.y / scalar;
	result.z = vector.z / scalar;

	return result;
}
Vec3& operator /= (Vec3& vector, real32 scalar)
{
	vector = vector / scalar;
	return vector;
}

// Vec4
Vec4 operator + (Vec4 vectorA, Vec4 vectorB)
{
	Vec4 result;

	result.x = vectorA.x + vectorB.x;
	result.y = vectorA.y + vectorB.y;
	result.z = vectorA.z + vectorB.z;
	result.w = vectorA.w + vectorB.w;
	
	return result;
}
Vec4 operator + (Vec4 vector, real32 constant)
{
	Vec4 result;

	result.x = vector.x + constant;
	result.y = vector.y + constant;
	result.z = vector.z + constant;
	result.w = vector.w + constant;
	
	return result;
}
Vec4& operator += (Vec4& vectorA, Vec4 vectorB)
{
	vectorA.x += vectorB.x;
	vectorA.y += vectorB.y;
	vectorA.z += vectorB.z;
	vectorA.w += vectorB.w;


	return vectorA;
}
Vec4& operator += (Vec4& vector, real32 constant)
{
	vector = vector + constant;
	return vector;
}

Vec4 operator - (Vec4 vectorA, Vec4 vectorB)
{
	Vec4 result;

	result.x = vectorA.x - vectorB.x;
	result.y = vectorA.y - vectorB.y;
	result.z = vectorA.z - vectorB.z;
	result.w = vectorA.w - vectorB.w;
	
	return result;
}
Vec4 operator - (Vec4 vector, real32 constant)
{
	Vec4 result;

	result.x = vector.x - constant;
	result.y = vector.y - constant;
	result.z = vector.z - constant;
	result.w = vector.w - constant;
	
	return result;
}
Vec4& operator -= (Vec4& vectorA, Vec4 vectorB)
{
	vectorA.x -= vectorB.x;
	vectorA.y -= vectorB.y;
	vectorA.z -= vectorB.z;
	vectorA.w -= vectorB.w;

	return vectorA;
}
Vec4& operator -= (Vec4& vector, real32 constant)
{
	vector = vector - constant;
	return vector;
}
Vec4 operator - (Vec4 vector)
{
	Vec4 result;
	result.x = -vector.x;
	result.y = -vector.y;
	result.z = -vector.z;
	result.w = -vector.w;

	return result;
}


Vec4 operator * (real32 scalar, Vec4 vector)
{
	Vec4 result;

	result.x = scalar * vector.x;
	result.y = scalar * vector.y;
	result.z = scalar * vector.z;
	result.w = scalar * vector.w;

	return result;
}
Vec4 operator * (Vec4 vector, real32 scalar)
{
	return scalar * vector;
}
Vec4& operator *= (Vec4& vector, real32 scalar)
{
	vector = vector * scalar;
	return vector;
}

Vec4 operator / (Vec4 vector, real32 scalar)
{
	Vec4 result;

	result.x = vector.x / scalar;
	result.y = vector.y / scalar;
	result.z = vector.z / scalar;
	result.w = vector.w / scalar;

	return result;
}
Vec4& operator /= (Vec4& vector, real32 scalar)
{
	vector = vector / scalar;
	return vector;
}

//
// FUNCTIONS
//

// Vec2
real32 dot(Vec2 vectorA, Vec2 vectorB)
{
	real32 result = vectorA.x * vectorB.x + vectorA.y * vectorB.y;
	return result;
}
real32 lengthSquared(Vec2 v)
{
	real32 result = v.x * v.x + v.y * v.y;
	return result;
}
real32 length(Vec2 v)
{
	real32 result = sqrt(lengthSquared(v));
	return result;
}
Vec2 normalize(Vec2 v)
{
	Vec2 result = v / length(v);
	return result;
}

// Vec3
real32 dot(Vec3 vectorA, Vec3 vectorB)
{
	real32 result =
		vectorA.x * vectorB.x +
		vectorA.y * vectorB.y +
		vectorA.z * vectorB.z;
	
	return result;
}
Vec3 cross(Vec3 vectorA, Vec3 vectorB)
{
	Vec3 result;

	result.x = vectorA.y * vectorB.z - vectorA.z * vectorB.y;
	result.y = vectorA.z * vectorB.x - vectorA.x * vectorB.z;
	result.z = vectorA.x * vectorB.y - vectorA.y * vectorB.x;

	return result;
}
real32 lengthSquared(Vec3 v)
{
	real32 result = v.x * v.x + v.y * v.y + v.z * v.z;
	return result;
}
real32 length(Vec3 v)
{
	real32 result = sqrt(lengthSquared(v));
	return result;
}
Vec3 normalize(Vec3 v)
{
	Vec3 result = v / length(v);
	return result;
}

// Vec4
real32 dot(Vec4 vectorA, Vec4 vectorB)
{
	real32 result =
		vectorA.x * vectorB.x +
		vectorA.y * vectorB.y +
		vectorA.z * vectorB.z +
		vectorA.w * vectorB.w;
	
	return result;
}
real32 lengthSquared(Vec4 v)
{
	real32 result = v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w;
	return result;
}
real32 length(Vec4 v)
{
	real32 result = sqrt(lengthSquared(v));
	return result;
}
Vec4 normalize(Vec4 v)
{
	Vec4 result = v / length(v);
	return result;
}


//=== Slightly more advanced functions ===//

Vec3 project(Vec3 vectorA, Vec3 vectorB)
{
	Vec3 result;

	real32 dotValue = dot(vectorA, vectorB);
	result = (vectorB * dotValue) / lengthSquared(vectorB);
	
	return result;
}

real32 distanceSquared(Vec3 vector, Plane plane)
{
	ALS_ASSERT(isNormal(plane.normal));

	Vec3 toVector = vector - plane.point;
	Vec3 projection = project(toVector, plane.normal);
	real32 result = lengthSquared(projection);
	
	return result;
}

real32 distance(Vec3 vector, Plane plane)
{
	real32 result = distanceSquared(vector, plane);
	result = sqrt(result);
	
	return result;
}

real32 distance(Vec2 vectorA, Vec2 vectorB)
{
	real32 result = length(vectorA - vectorB);
	return result;
}

real32 distance(Vec3 vectorA, Vec3 vectorB)
{
	real32 result = length(vectorA - vectorB);
	return result;
}

real32 distance(Vec4 vectorA, Vec4 vectorB)
{
	real32 result = length(vectorA - vectorB);
	return result;
}

bool32 equals(Vec3 vectorA, Vec3 vectorB)
{
	real32 deltaX = abs(vectorA.x - vectorB.x);
	real32 deltaY = abs(vectorA.y - vectorB.y);
	real32 deltaZ = abs(vectorA.z - vectorB.z);

	return deltaX < EPSILON && deltaY < EPSILON && deltaZ < EPSILON;
}

bool32 directionEquals(Vec3 vectorA, Vec3 vectorB)
{
	vectorA = normalize(vectorA);
	vectorB = normalize(vectorB);
	
	return equals(vectorA, vectorB);
}

Vec3 project(Vec3 vector, Plane plane)
{
	ALS_ASSERT(isNormal(plane.normal));
	
	Vec3 vProjNormal = project(vector, plane.normal);
	Vec3 result = vector - vProjNormal;

	return result;
}

Vec2 zeroVec2()
{
	Vec2 result = vector2(0, 0);
	return result;
}

Vec3 zeroVec3()
{
	Vec3 result = vector3(0, 0, 0);
	return result;
}

Vec4 zeroVec4()
{
	Vec4 result = vector4(0, 0, 0, 0);
	return result;
}

bool32 isZeroVector(Vec2 v)
{
	bool32 result = (v.x == 0) && (v.y == 0);
	
	return result;
}

bool32 isZeroVector(Vec3 v)
{
	bool32 result = (v.x == 0) && (v.y == 0) && (v.z == 0);
	
	return result;
}

bool32 isZeroVector(Vec4 v)
{
	bool32 result = (v.x == 0) && (v.y == 0) && (v.z == 0) && (v.w == 0);
	
	return result;
}

bool32 isNormal(Vec2 v)
{
    bool32 result;
	result = FLOAT_EQ(lengthSquared(v), 1.0f, 0.01);

	return result;
}

bool32 isNormal(Vec3 v)
{
	bool32 result;
	result = FLOAT_EQ(lengthSquared(v), 1.0f, 0.01);

	return result;
}

bool32 isNormal(Vec4 v)
{
	bool32 result;
	result = FLOAT_EQ(lengthSquared(v), 1.0f, 0.01);
	
	return result;
}

bool32 isOrthogonal(Vec3 vectorA, Vec3 vectorB)
{
	ALS_ASSERT(!isZeroVector(vectorA) && !isZeroVector(vectorB));
	real32 result = abs(dot(vectorA, vectorB));

	/* return result < .0001; */
	return result < .02; // WHYYYYYYYYYYY
}

bool32 isCollinear(Vec3 pointA, Vec3 pointB, Vec3 pointC)
{
	Vec3 ab = pointB - pointA;
	Vec3 ac = pointC - pointA;
	
	// We will check dx/dz and dy/dz -- and if they are the same from
	// A -> B and A -> C, then the points are collinear. However,
	// there is 1 caveat.  If all the points have the same z value,
	// then we will get divide by 0 errors.  In this situation, we
	// just need to check dy/dx, i.e., the slope of the lines, since
	// they are on the same plane.
	if(abs(ab.z) < EPSILON)
	{
		if(abs(ac.z) < EPSILON)
		{
			// 2D collinear test! Same caveat where delta X may be 0,
			// so check for that first.

			if(abs(ab.x) < EPSILON || abs(ac.x) < EPSILON)
			{
				if(abs(ab.x) < EPSILON && abs(ac.x) < EPSILON)
				{
					return true;
				}
				else
				{
					return false;
				}
			}

			// All points have different X values
			real32 abDyDx = ab.y / ab.x;
			real32 acDyDx = ac.y / ac.x;

			return FLOAT_EQ(abDyDx, acDyDx, EPSILON);
		}
		else
		{
			return false;
		}
	}

	// All points have different Z values
	real32 abDxDz = ab.x / ab.z;
	real32 abDyDz = ab.y / ab.z;

	real32 acDxDz = ac.x / ac.z;
	real32 acDyDz = ac.y / ac.z;

	return FLOAT_EQ(abDxDz, acDxDz, EPSILON) && FLOAT_EQ(abDyDz, acDyDz, EPSILON);
}

Quaternion axisAngle(Vec3 axis, real32 degrees)
{
	axis = normalize(axis);
	
	Quaternion q;
	real32 sinHalfAngle = sin(TO_RAD(degrees / 2.0f));
	
	q.x = axis.x * sinHalfAngle;
	q.y = axis.y * sinHalfAngle;
	q.z = axis.z * sinHalfAngle;
	q.w = cos(TO_RAD(degrees / 2.0f));

	return q;
}

Quaternion identityQuaternion()
{
	Quaternion q;
	q.x = q.y = q.z = 0;
	q.w = 1;

	return q;
}

Quaternion rotateVector(Vec3 initial, Vec3 target)
{
	Quaternion result;

	Vec3 initialNormal = normalize(initial);
	Vec3 targetNormal = normalize(target);

	if (equals(initialNormal, targetNormal)) {
		return identityQuaternion();
	}

	Vec3 axis = cross(initial, target);
	real32 angle = TO_DEG(
		acos(
			dot(
				normalize(initial),
				normalize(target)
				)
			)
		);
	
	result = axisAngle(axis, angle);

	return result;
}

Mat3 identityMatrix3()
{
	Mat3 result = {};
	
	result[0][0] = 1;
	result[1][1] = 1;
	result[2][2] = 1;

	return result;
}

Mat4 identityMatrix4()
{
	Mat4 result = {};
	
	result[0][0] = 1;
	result[1][1] = 1;
	result[2][2] = 1;
	result[3][3] = 1;

	return result;
}

// Taken from http://answers.unity3d.com/questions/467614/what-is-the-source-code-of-quaternionlookrotation.html
// Don't really understand how it works......
Quaternion lookRotation(Vec3 forward, Vec3 up)
{
	// ALS_ILLEGAL;
	ALS_ASSERT(isOrthogonal(forward, up));
			   
	forward = normalize(forward);
 
	Vec3 vector = normalize(forward);
	Vec3 vector2 = normalize(cross(up, vector));
	Vec3 vector3 = cross(vector, vector2);
	real32 m00 = vector2.x;
	real32 m01 = vector2.y;
	real32 m02 = vector2.z;
	real32 m10 = vector3.x;
	real32 m11 = vector3.y;
	real32 m12 = vector3.z;
	real32 m20 = vector.x;
	real32 m21 = vector.y;
	real32 m22 = vector.z;
 
 
	real32 num8 = (m00 + m11) + m22;
	
	Quaternion quaternion;
	
	if (num8 > 0.0f)
	{
		real32 num = (real32)sqrt(num8 + 1.0f);
		quaternion.w = num * 0.5f;
		num = 0.5f / num;
		quaternion.x = (m12 - m21) * num;
		quaternion.y = (m20 - m02) * num;
		quaternion.z = (m01 - m10) * num;
		return quaternion;
	}
	if ((m00 >= m11) && (m00 >= m22))
	{
		real32 num7 = (real32)sqrt(((1.0f + m00) - m11) - m22);
		real32 num4 = 0.5f / num7;
		quaternion.x = 0.5f * num7;
		quaternion.y = (m01 + m10) * num4;
		quaternion.z = (m02 + m20) * num4;
		quaternion.w = (m12 - m21) * num4;
		return quaternion;
	}
	if (m11 > m22)
	{
		real32 num6 = (real32)sqrt(((1.0f + m11) - m00) - m22);
		real32 num3 = 0.5f / num6;
		quaternion.x = (m10+ m01) * num3;
		quaternion.y = 0.5f * num6;
		quaternion.z = (m21 + m12) * num3;
		quaternion.w = (m20 - m02) * num3;
		return quaternion; 
	}
	real32 num5 = (real32)sqrt(((1.0f + m22) - m00) - m11);
	real32 num2 = 0.5f / num5;
	quaternion.x = (m20 + m02) * num2;
	quaternion.y = (m21 + m12) * num2;
	quaternion.z = 0.5f * num5;
	quaternion.w = (m01 - m10) * num2;
	
	return normalize(quaternion);
}

// http://answers.unity3d.com/questions/372371/multiply-quaternion-by-vector3-how-is-done.html
Mat3 rotationMatrix3(Quaternion q)
{
	Mat3 result;

	result[0][0] = 1 - (2 * q.y * q.y) - (2 * q.z * q.z);
	result[0][1] = 2 * q.x * q.y - 2 * q.z * q.w;
	result[0][2] = 2 * q.x * q.z + 2 * q.y * q.w;

	result[1][0] = 2 * q.x * q.y + 2 * q.z * q.w;
	result[1][1] = 1 - (2 * q.x * q.x) - (2 * q.z * q.z);
	result[1][2] = 2 * q.y * q.z - 2 * q.x * q.w;

	result[2][0] = 2 * q.x * q.z - 2 * q.y * q.w;
	result[2][1] = 2 * q.y * q.z + 2 * q.x * q.w;
	result[2][2] = 1 - (2 * q.x * q.x) - (2 * q.y * q.y);

	return result;
}

Mat4 rotationMatrix4(Quaternion q)
{
	Mat4 result;
	Mat3 m = rotationMatrix3(q);

	result[0][0] = m[0][0];
	result[0][1] = m[0][1];
	result[0][2] = m[0][2];
	result[0][3] = 0;
	
	result[1][0] = m[1][0];
	result[1][1] = m[1][1];
	result[1][2] = m[1][2];
	result[1][3] = 0;
	
	result[2][0] = m[2][0];
	result[2][1] = m[2][1];
	result[2][2] = m[2][2];
	result[2][3] = 0;

	result[3][0] = 0;
	result[3][1] = 0;
	result[3][2] = 0;
	result[3][3] = 1;
	
	return result;
}

Mat3 operator * (Mat3 left, Mat3 right)
{
	Mat3 result;

	const int matDimension = 3;

	for(int down = 0; down < matDimension; down++)
	{
		for(int across = 0; across < matDimension; across++)
		{
			real32 sum = 0;
			
			for(int i = 0; i < matDimension; i++)
			{
				sum += (left[down][i] * right[i][across]);
			}
			
			result[down][across] = sum;
		}
	}
	
	return result;
}

Mat4 operator * (Mat4 left, Mat4 right)
{
	Mat4 result;

	const int matDimension = 4;

	for(int down = 0; down < matDimension; down++)
	{
		for(int across = 0; across < matDimension; across++)
		{
			real32 sum = 0;
			
			for(int i = 0; i < matDimension; i++)
			{
				sum += (left[down][i] * right[i][across]);
			}
			
			result[down][across] = sum;
		}
	}
	
	return result;
}

Vec3 operator * (Mat3 m, Vec3 v)
{
	Vec3 result;

	result.x = m[0][0] * v.x   +   m[0][1] * v.y   +   m[0][2] * v.z;
	result.y = m[1][0] * v.x   +   m[1][1] * v.y   +   m[1][2] * v.z;
	result.z = m[2][0] * v.x   +   m[2][1] * v.y   +   m[2][2] * v.z;

	return result;
}

Vec4 operator * (Mat4 m, Vec4 v)
{
	Vec4 result;

	result.x = m[0][0] * v.x   +   m[0][1] * v.y   +   m[0][2] * v.z   +   m[0][3] * v.w;
	result.y = m[1][0] * v.x   +   m[1][1] * v.y   +   m[1][2] * v.z   +   m[1][3] * v.w;
	result.z = m[2][0] * v.x   +   m[2][1] * v.y   +   m[2][2] * v.z   +   m[2][3] * v.w;
	result.w = m[3][0] * v.x   +   m[3][1] * v.y   +   m[3][2] * v.z   +   m[3][3] * v.w;

	return result;
}

Quaternion operator * (Quaternion a, Quaternion b)
{
	Quaternion result;

	result.x = (a.w * b.x)   +   (a.x * b.w)   +   (a.y * b.z)   -   (a.z * b.y);
	result.y = (a.w * b.y)   +   (a.y * b.w)   +   (a.z * b.x)   -   (a.x * b.z);
	result.z = (a.w * b.z)   +   (a.z * b.w)   +   (a.x * b.y)   -   (a.y * b.x);
	result.w = (a.w * b.w)   -   (a.x * b.x)   -   (a.y * b.y)   -   (a.z * b.z);

	return result;
}

Vec3 operator * (Quaternion quaternion, Vec3 vector)
{
	Vec3 result = rotationMatrix3(quaternion) * vector;

	return result;
}

Vec3 upVector(Quaternion quaternion)
{
	Vec3 result = normalize(quaternion) * vector3(0, 1, 0);
	return result;
}

Vec3 forwardVector(Quaternion quaternion)
{
	Vec3 result = normalize(quaternion) * vector3(0, 0, 1);
	return result;
}

Vec3 rightVector(Quaternion quaternion)
{
	// TODO: FIX THIS SHIT
	// leave this uncommented so I can step in with a debugger and
	// check the value before I hack around it by doing cross product
	Vec3 result = normalize(quaternion) * vector3(1, 0, 0);

	
	result = normalize(cross(forwardVector(quaternion), upVector(quaternion)));
	return result; // FIX THIS SHIT
}

Mat3 transpose(Mat3 m)
{
	Mat3 result;
	
	result[0][0] = m[0][0];
	result[0][1] = m[1][0];
	result[0][2] = m[2][0];

	result[1][0] = m[0][1];
	result[1][1] = m[1][1];
	result[1][2] = m[2][1];

	result[2][0] = m[0][2];
	result[2][1] = m[1][2];
	result[2][2] = m[2][2];

	return result;
}

Mat4 transpose(Mat4 m)
{
	Mat4 result;
	
	result[0][0] = m[0][0];
	result[0][1] = m[1][0];
	result[0][2] = m[2][0];
	result[0][3] = m[3][0];

	result[1][0] = m[0][1];
	result[1][1] = m[1][1];
	result[1][2] = m[2][1];
	result[1][3] = m[3][1];

	result[2][0] = m[0][2];
	result[2][1] = m[1][2];
	result[2][2] = m[2][2];
	result[2][3] = m[3][2];

	result[3][0] = m[0][3];
	result[3][1] = m[1][3];
	result[3][2] = m[2][3];
	result[3][3] = m[3][3];

	return result;
}

Mat4 translationMatrix(Vec3 translation)
{
	Mat4 result = {};
	
	result[0][3] = translation.x;
	result[1][3] = translation.y;
	result[2][3] = translation.z;

	result[0][0] = 1;
	result[1][1] = 1;
	result[2][2] = 1;
	result[3][3] = 1;

	return result;
}

Mat4 scalingMatrix(Vec3 scale)
{
	Mat4 result = {};

	result[0][0] = scale.x;
	result[1][1] = scale.y;
	result[2][2] = scale.z;
	result[3][3] = 1;

	return result;
}

