struct LevelSlice
{
	real32 botY;
	real32 topY;
};

struct Level : Entity
{
	int32 sliceCount;
	LevelSlice slices[globals.capacities.maxLevelSliceCount];

	real32 radius()
	{
		SphereCollider* collider = (SphereCollider*)(this->collider);
		return collider->radius;
	}
};

Level* constructLevel(Vec3 position, real32 radius, int32 sliceCount)
{
	ALS_ASSERT(radius > 0);
	ALS_ASSERT(sliceCount > 0);
	ALS_ASSERT(sliceCount <= globals.capacities.maxLevelSliceCount);

	Level* result;
	ALS_ALLOC(Level, result, 1);
	
	result->position = position;
	result->sliceCount = sliceCount;

	result->orientation = identityQuaternion();

	result->colliderType = SPHERE;
	result->collider = constructSphereCollider(radius, result);

	result->flags = 0;

	for(int i = 0; i < sliceCount; i++)
	{
		real32 botLat = -90 + (180 / (real32)sliceCount) * i;
		real32 topLat = -90 + (180 / (real32)sliceCount) * (i + 1);
			
		result->slices[i].botY = result->radius() * sin(TO_RAD(botLat));
		result->slices[i].topY = result->radius() * sin(TO_RAD(topLat));
	}

	return result;
}

LatLong posToLatLong(real32 radius, Vec3 pos)
{
	// Project up or down to be on surface of sphere
	pos = normalize(pos) * radius;
		
	LatLong result;

	result.latitude = TO_DEG(asin(pos.y / radius));

	if(FLOAT_EQ(abs(result.latitude), 90.0f, EPSILON))
	{
		result.longitude = 0;
	}
		
	result.longitude = TO_DEG(atan2(pos.x, pos.z));

	ALS_ASSERT(abs(result.latitude) <= 90.0f + EPSILON);
	ALS_ASSERT(abs(result.longitude) <= 180.0f + EPSILON);
		
	return result;
}

LatLong posToLatLong(Level* level, Vec3 pos)
{
	LatLong result = posToLatLong(level->radius(), pos);
	return result;
}

Vec3 latLongToPos(real32 radius, LatLong l)
{
	Vec3 result;

	result.x = radius * cos(TO_RAD(l.longitude)) * sin(TO_RAD(l.latitude));
	result.y = radius * sin(TO_RAD(l.longitude)) * sin(TO_RAD(l.latitude));
	result.z = radius * cos(TO_RAD(l.latitude));

	return result;
}

Vec3 latLongToPos(Level *level, LatLong l)
{
	Vec3 result = latLongToPos(level->radius(), l);
	return result;
}
