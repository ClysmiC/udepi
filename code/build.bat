@echo off

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

SET CompilerFlags= /Zi /I w:\code
SET LinkerFlags= w:\lib\glfw3dll.lib w:\lib\glew32.lib opengl32.lib glu32.lib /INCREMENTAL:NO

REM set up VC++ vars
call w:\code\shell_setup.bat

REM /f >nul 2>&1 pipes the output to null, so it isn't seen
taskkill /IM "als_game.exe" /f >nul 2>&1

cl %CompilerFlags% w:\code\als_game.cpp /link %LinkerFlags%

popd
