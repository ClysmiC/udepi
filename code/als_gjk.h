//
// GJK
//
struct GjkSimplex
{
	// Simplex can have up to 4 points, but not necessarily always 4
	// Most "recent" point is always points[count-1]
	Vec3 points [4];
	
	uint32 count = 0;

	// Adds a point to the simplex.
	void add(Vec3 valueToAdd)
	{
		ALS_ASSERT(count < 4);
		
		points[count] = valueToAdd;
		count++;
	}

	void clear()
	{
		count = 0;
	}

	// Order doesn't matter when reconstructing simplex, since this is done
	// in gjkDoSimplex(...), and an add(...) call will always occur
	// in gjkCollisionTest(...) before the next gjkDoSimplex(...)
	void reconstruct(Vec3* points, uint32 numPoints)
	{
		clear();
		for(int i = 0; i < numPoints; i++)
		{
			this->points[i] = points[i];
		}

		this->count = numPoints;
	}
};

struct GjkResult
{
	bool32 collides;
	Vec3 penetration;
};


//
// EPA
//
struct EpaSimplex
{
	const uint32 maxCount = 64;
	Triangle faces [64]; // Arbitrary number... increase if needed

	uint32 count;

	void initFromGjkSimplex(GjkSimplex s)
	{
		ALS_ASSERT(s.count == 4);

		Vec3 a = s.points[3];
		Vec3 b = s.points[2];
		Vec3 c = s.points[1];
		Vec3 d = s.points[0];

		count = 0;
		
		add(triangle(a, c, b));
		add(triangle(a, d, c));
		add(triangle(a, b, d));
		add(triangle(b, c, d));
	}

	real32 closestFaceToOrigin(Triangle* t, uint32* index)
	{
		Vec3 origin = vector3(0, 0, 0);
		ALS_ASSERT(count <= maxCount);
		
		// We can "cheat" and project the point onto the plane defined by the triangle
		// because if this gives us a shorter distance than it would be to the triangle
		// then we are guaranteed to have a different face be even closer.

		Triangle closestTriangle = faces[0];
		real32 closestDistanceSq = 10000000;
		uint32 closestIndex = 0;
		
		for(uint32 i = 0; i < count; i++)
		{
			Triangle triangle = faces[i];
			Plane triPlane = plane(triangle.a, triangle.normal());

			real32 distanceSq = distanceSquared(
				vector3(0, 0, 0),
				triPlane
				);
			
			if(distanceSq < closestDistanceSq)
			{
				closestDistanceSq = distanceSq;
				closestTriangle = triangle;
				closestIndex = i;
			}
		}

		ALS_ASSERT(closestDistanceSq < 10000000);
		*t = closestTriangle;
		*index = closestIndex;

		return sqrt(closestDistanceSq);
	}

	void removeUnordered(uint32 index)
	{
		ALS_ASSERT(index < count);
		
		faces[index] = faces[count-1];
		count--;
	}

	void add(Triangle triangle)
	{
		ALS_ASSERT(count < maxCount);

		{
			Vec3 ao = -triangle.a;
			
			/* ALS_ASSERT(dot(ao, triangle.normal()) <= 0.0f); */
		}

		faces[count] = triangle;
		count++;
	}

	void extend(Vec3 newPoint)
	{
		Edge3 edges[64 * 3];
		uint32 edgeCount = 0;
		
		for(int i = 0; i < count; i++)
		{
			Triangle t = faces[i];
			Vec3 normal = t.normal();

			Vec3 aToNew = newPoint - t.a;
			if(dot(normal, aToNew) > 0)
			{
				// This triangle is "visible", thus must be removed
				
				removeUnordered(i);
				i--;

				// Wind CCW!
				Edge3 ab = edge(t.a, t.b);
				Edge3 bc = edge(t.b, t.c);
				Edge3 ca = edge(t.c, t.a);

				bool32 abDup = false;
				bool32 bcDup = false;
				bool32 caDup = false;
				
				for(int j = 0; j < edgeCount; j++)
				{
					Edge3 edge = edges[j];
					bool32 removeEdge = false;
					
					if(equals(edge.p0, ab.p1) && equals(edge.p1, ab.p0))
					{
						abDup = true;
						removeEdge = true;
					}

					if(equals(edge.p0, bc.p1) && equals(edge.p1, bc.p0))
					{
						bcDup = true;
						removeEdge = true;
					}

					if(equals(edge.p0, ca.p1) && equals(edge.p1, ca.p0))
					{
						caDup = true;
						removeEdge = true;
					}
						
					// The edge is already in our list, which means it must be removed
					if(removeEdge)
					{
						edges[j] = edges[edgeCount - 1];
						edgeCount--;
						j--;
					}
				}

				if(!abDup)
				{
					edges[edgeCount] = ab;
					edgeCount++;
				}

				if(!bcDup)
				{
					edges[edgeCount] = bc;
					edgeCount++;
				}

				if(!caDup)
				{
					edges[edgeCount] = ca;
					edgeCount++;
				}
			}
		}


		for(int i = 0; i < edgeCount; i++)
		{
			Edge3 edge = edges[i];
			Triangle newTri = triangle(edge.p0, edge.p1, newPoint);
			add(newTri);
		}
	}
};
