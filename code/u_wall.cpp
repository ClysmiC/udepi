struct Wall : Entity
{
	float height()
	{
		WallCollider* collider = (WallCollider*)this->collider;
		return collider->height;
	}

	float xDim()
	{
		WallCollider* collider = (WallCollider*)this->collider;
		return collider->dimensions.x;
	}

	float yDim()
	{
		WallCollider* collider = (WallCollider*)this->collider;
		return collider->dimensions.y;
	}
};

Wall* constructWall(Vec2 dimensions, real32 height, Vec3 centerPosition, Quaternion orientation)
{
	Wall* result;
	ALS_ALLOC(Wall, result, 1);

	result->colliderType = ROUND_PYRAMID;
	result->collider = constructWallCollider(dimensions, height, result);

	result->position = centerPosition;
	result->orientation = orientation;

	return result;
}

Wall* constructWall(Vec2 dimensions, real32 height, Vec3 centerPosition, Level* level)
{
	Vec3 northPole = vector3(0, level->radius(), 0);
	
	// This rotation gets it to have the proper "up" and "down"
	Quaternion rotationNeeded = rotateVector(northPole, centerPosition);

	// Vec3 defaultForward = vector3(0, 0, 1);
	// Vec3 forwardBeforeAligning = rotationNeeded * defaultForward;
	// Vec3 targetForward = northPole - centerPosition;

	// This rotation "twists" it so that it's forward vector is pointing towards
	// the North Pole
	
	Wall* result;
	ALS_ALLOC(Wall, result, 1);

	result->colliderType = ROUND_PYRAMID;
	result->collider = constructWallCollider(dimensions, height, result);

	result->position = centerPosition;
	result->orientation = rotationNeeded;;

	Vec3 forward = forwardVector(result->orientation);
	Vec3 targetForward = northPole - centerPosition;
	
	Quaternion alignmentRotationNeeded = rotateVector(forward, targetForward);
	result->orientation = result->orientation * alignmentRotationNeeded;

	return result;
}
