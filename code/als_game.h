#include <GL/glew.h>

#define GLFW_DLL
#include <GLFW/glfw3.h>

#include <math.h>
#include <stdlib.h>
#include <cstdio>
#include <time.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb/stb_truetype.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb/stb_image_write.h"

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef int32 bool32;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
    
typedef float real32;
typedef double real64;

#define ALS_ASSERT(x) if(!(x)) { char* foo = 0; *foo = 10; }
#define ALS_ILLEGAL ALS_ASSERT(false)
#define ALS_ALLOC(type, name, count) name = (type*)malloc(sizeof(type) * count)
#define ALS_ARRAY_LEN(name) sizeof(name) / sizeof((name)[0])
#define ALS_SWAP(ptr1, ptr2, type) { type temp = *(ptr1); *(ptr1) = *(ptr2); *(ptr2) = temp; }

#define PI 3.14159265359f
#define EPSILON 0.00001f

#define TO_RAD(x) ((x)* PI / 180.0f)
#define TO_DEG(x) ((x)* 180.0f/ PI)

#define FLOAT_EQ(x, y, epsilon) (abs((x) - (y)) < epsilon)


int powi(int base, int power)
{
	int result = 1;
	for(int i = 0; i < power; i++)
	{
		result *= base;
	}

	return result;
}
		
#include "u_globals.h"
		
/** GLOBAL PRIMITIVES **/
bool keys[1024] = {};
bool lastKeys[1024] = {};

GLuint basicShader;
GLuint debugShader; 
GLuint textShader;

Globals globals;

/** UNITY BUILD **/

#include "als_glfw.h"
		
#include "als_math.h"
#include "als_camera.h"
		
#include "u_latlong.h"


#include "u_collider.cpp"
#include "u_entity.cpp"
#include "u_level.h"
#include "u_udepi.cpp"
#include "u_wall.cpp"

#include "als_gjk.h"
#include "als_gjk.cpp"


#include "als_graphics.h"
#include "als_graphics.cpp"

/** GLOBALS (CUSTOM TYPES) **/
Scene scene;
