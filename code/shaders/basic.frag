#version 330 core
	
out vec4 color;

in vec3 norm;
in vec3 pos;

struct Light {
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

uniform Light light;
uniform Material material;

uniform vec3 viewPos;

void main()
{
    vec3 ambientColor = material.ambient * light.ambient;
	
	vec3 normal = normalize(norm);
	vec3 lightDir = normalize(light.position - pos);

	vec3 diffuseColor = material.diffuse *
		max(dot(lightDir, normal), 0.0f) * light.diffuse;
	
	vec3 viewDir = normalize(viewPos - pos);
	vec3 reflectDir = reflect(-lightDir, normal);

	float specVal = pow(max(dot(reflectDir, viewDir), 0.0f), material.shininess);
	
	vec3 specularColor = specVal * material.specular * light.specular;
	
	color = vec4((ambientColor + diffuseColor + specularColor), 1.0);
}
