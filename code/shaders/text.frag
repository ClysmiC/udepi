#version 330 core

uniform sampler2D fontTexture;

in vec2 _texCoord;
in vec4 _targetColor;
out vec4 color;

void main()
{
	// Due to weirdness with how OpenGL handles single channel
	// textures, the value we want is in the red channel.
	float texColor = texture(fontTexture, _texCoord).r;
	
	color = vec4(
		texColor * _targetColor.r,
		texColor * _targetColor.g,
		texColor * _targetColor.b,
		texColor * _targetColor.a
	);
}
