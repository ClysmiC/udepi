#version 330 core

layout (location = 0) in vec2 screenPosition;
layout (location = 1) in vec2 texCoord;

uniform vec2 windowSize;
uniform vec4 targetColor;

out vec2 _texCoord;
out vec4 _targetColor;

void main()
{
	vec2 normalizedPosition;

	// Map position from [0, windowSize] to [-1, 1]
	normalizedPosition.x = clamp(
		-1 + 2 * (screenPosition.x / windowSize.x),
		-1,
		1
	);

	normalizedPosition.y = clamp(
		-1 + 2 * (screenPosition.y / windowSize.y),
		-1,
		1
	);
	
	// Flip upside-down font weirdness
	normalizedPosition.y = -normalizedPosition.y;

	gl_Position = vec4(normalizedPosition, .95, 1);;
	
	_texCoord = texCoord;
	_targetColor = targetColor;
}
