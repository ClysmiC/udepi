#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 pos;
out vec3 norm;

void main()
{
	gl_Position = projection * view * model * vec4(position, 1.0f);

	// Note: pos is the position in world space
	pos = vec3(model * vec4(position, 1.0f));

	// transpose of inverse of model matrix is needed to
	// not have adverse affects by non-uniform scales and translates
	norm = mat3(transpose(inverse(model))) * normal;
}
