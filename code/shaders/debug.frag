#version 330 core
	
out vec4 color;
in vec4 pointColor;

void main()
{
	color = pointColor;
}
