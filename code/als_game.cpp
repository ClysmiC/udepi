#include "als_game.h"

uint64 frameCounter = 0;
real32 recentFrames[16] = {};
uint32 recentFrameIndex = 0;







Vec3 xAxis = vector3(1, 0, 0);
Vec3 yAxis = vector3(0, 1, 0);
Vec3 zAxis = vector3(0, 0, 1);

GLenum glError;

int main()
{
	// INIT WINDOW
	GLFWwindow* window;
	uint32 windowWidth = 1024;
	uint32 windowHeight = 720;


	if(initWindow(&window, windowWidth, windowHeight) < 0)
	{
		return -1;
	}
	
	// INIT BASIC SHADERS
	{
		basicShader = compileShader("../code/shaders/basic.vert", "../code/shaders/basic.frag");
		debugShader = compileShader("../code/shaders/debug.vert", "../code/shaders/debug.frag");
		textShader = compileShader("../code/shaders/text.vert", "../code/shaders/text.frag");
	
		if(!basicShader)
		{
			printf("Failed to compile basic shader. Exiting.\n");
			return -1;
		}

		if(!debugShader)
		{
			printf("Failed to compile debug shader. Exiting.\n");
			return -1;
		}

		if(!textShader)
		{
			printf("Failed to compile line shader. Exiting.\n");
			return -1;
		}
	}

	// INIT CAMERA
	// TODO -- figure out how camera/scene/level relate and if they can be compacted
	real32 cameraDistance = 26;
	Camera camera;
	{
		camera.position = vector3(0.0f, 0.0f, cameraDistance);
		
		camera.target = vector3(0.0f, 0.0f, 0.0f);
		camera.orientation = lookRotation(camera.target - camera.position,
										  yAxis);
		camera.fov = 90.0f;
		camera.aspectRatio = windowWidth / (real32)windowHeight;

		camera.near = 0.1f;
		camera.far = 100.0f;
	}

	// just for debugging -- this is the one we actually look through
	// other camera is the camera that controls get determined by
	// Camera realCam = camera;
	// bool32 realCamAttached = true;

	// INIT SCENE
	// Scene scene;
	{
		scene.lightCount = 1;
		scene.lights[0].position = vector3(2.0f, 0.0f, -6.0f);
		// scene.lights[0].ambient = vector3(0.2f, 0.2f, 0.2f);
		scene.lights[0].ambient = vector3(.5, .5, .5); // TEMP
		scene.lights[0].diffuse = vector3(0.5f, 0.5f, 0.5f);
		scene.lights[0].specular = vector3(1.0f, 1.0f, 1.0f);

		scene.camera = &camera;

		scene.projection = camera.projectionMatrix();
		scene.view = camera.viewMatrix();
	
		initCommonShapes(&scene.commonShapes);
		_initFontTexture(&scene);
	}

	// INIT LEVEL
	Level* level = constructLevel(
		vector3(0, 0, 0), // position
		16.0f,            // radius
		20);              // slice count

	real32 northSliceY = level->slices[level->sliceCount - 1].botY;
	real32 southSliceY = level->slices[0].topY;
	
	ALS_ASSERT(FLOAT_EQ(northSliceY, -southSliceY, EPSILON));
	
	Vec3 southPole = vector3(0, -1, 0) * level->radius();
	Vec3 northPole = vector3(0, 1, 0) * level->radius();
	
	// INIT PLAYER
	Udepi* player;
	{
		player = constructUdepi(
			level->radius() * zAxis,
			axisAngle(xAxis, 90)
		);
	}

	Wall* wall;
	{
		wall = constructWall(
			vector2(3, 3),
			1,
			level->radius() * normalize(vector3(1, 1, 1)),
			level
		);
	}

	Plane poleCameraPlane = {};
	Plane poleFlipPlane = {};

	Plane standingPlane = {};

	Vec3 forward = camera.up();
	Vec3 right = camera.right();

	standingPlane = plane(player->position, normalize(player->position));

	real32 beginFrame = glfwGetTime();


	//
	// TEST CODE FOR DETECTING INPUT DEVICES
	//
	
	// while(!glfwWindowShouldClose(window))
	// {
	// 	for(int i = 0; i < GLFW_JOYSTICK_LAST; i++)
	// 	{
	// 		glfwPollEvents();
	// 		int present = glfwJoystickPresent(GLFW_JOYSTICK_1 + i);

	// 		if(present)
	// 		{
	// 			printf("holla for a dolla");
	// 		}
	// 	}
	// }
	
	while(!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
		glClearColor(.5f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		real32 deltaT = .01666667;

		Vec2 inputDir = {};
		
		if(keys[GLFW_KEY_W])
		{
			inputDir += vector2(0, 1);
		}
		else if(keys[GLFW_KEY_S])
		{
			inputDir += vector2(0, -1);
		}

		if(keys[GLFW_KEY_A])
		{
			inputDir += vector2(-1, 0);
		}
		else if(keys[GLFW_KEY_D])
		{
			inputDir += vector2(1, 0);
		}

		if(keys[GLFW_KEY_SPACE] && !lastKeys[GLFW_KEY_SPACE])
		{
			player->velocity.up = player->jumpVelocity;
		}

		if(keys[GLFW_KEY_Q])
		{
			// realCamAttached = false;
			// realCam.position += .2 * camera.right();
		}
		else if(keys[GLFW_KEY_E])
		{
			// realCamAttached = false;
			// realCam.position -= .2 * camera.right();
		}
		
		if(keys[GLFW_KEY_LEFT_CONTROL])
		{
			// realCamAttached = true;
		}
		
		// if(realCamAttached)
		{
			// realCam = camera;
		}

		// UPDATE VELOCITY
		if(!isZeroVector(inputDir))
		{
			inputDir = normalize(inputDir);
			player->velocity.right += player->accel * inputDir.x;
			player->velocity.forward += player->accel * inputDir.y;

			if(lengthSquared(player->velocity.asVector2) > player->maxVelocitySquared)
			{
				player->velocity.asVector2 = player->maxVelocity * normalize(player->velocity.asVector2);
			}
		}

		real32 effectiveRadius = length(player->position);
		
		// MOVE LATERALLY
		if(abs(player->velocity.right) > 0.0f)
		{
			LatLong latLong = posToLatLong(level, player->position);
			real32 longitude = latLong.longitude;
			real32 latitude = latLong.latitude;

			real32 latitudeRadius = effectiveRadius * abs(cos(TO_RAD(latitude)));

			if(player->movementMode == ORBITAL)
			{
				// Movement speed is an arc length. To calculate the other
				// endpoint of the arc, find out the delta degrees that is
				// being moved.
				real32 hMovementDeltaDegrees = TO_DEG(deltaT * player->velocity.right / latitudeRadius);
				real32 targetLongitude = longitude;
				
				targetLongitude = longitude + camera.flipTest() * hMovementDeltaDegrees;

				real32 newX = latitudeRadius * sin(TO_RAD(targetLongitude));
				real32 newZ = latitudeRadius * cos(TO_RAD(targetLongitude));
			
				player->position.x = newX; 
				player->position.z = newZ;
			}
			else if(player->movementMode == NORMAL)
			{
				// cheese
				player->position += deltaT * player->velocity.right * right;
				player->position = effectiveRadius * normalize(player->position);
			}
			else
			{
				ALS_ILLEGAL;
			}
		}

		// MOVE FORWARD/BACKWARD
		if(abs(player->velocity.forward) > 0.0f)
		{
			// cheese
			// real32 effectiveRadius = length(player->bottomPoint());
			
			player->position += deltaT * player->velocity.forward * forward;
			player->position = effectiveRadius * normalize(player->position);
		}

		// MOVE DUE TO GRAVITY
		{
			Vec3 up = normalize(player->position);
			player->position += deltaT * player->velocity.up * up;

			player->velocity.up += deltaT * globals.physics.gravity;
		}

		// Collision resolve for gravity movement
		{
			GjkResult result;
			bool32 collides = gjkCollisionTest(player, level, &result);

			if(collides)
			{
				Vec3 penetrationVector = result.penetration;
				player->position -= penetrationVector;
				player->velocity.up = 0;
			}
		}

		bool32 playerMovedOtherThanUpDown = !isZeroVector(player->velocity.asVector2);

		if(playerMovedOtherThanUpDown)
		{
			standingPlane = player->getStandingPlane();

			if(player->movementMode == NORMAL &&
			   dot(player->position, poleFlipPlane.normal) < 0)
			{
				// CAMERA GETS FLIPPED
				poleFlipPlane.normal = -poleFlipPlane.normal;
			}

			// recalculate camera
			{

				Vec3 camRight;

				if(player->movementMode == ORBITAL)
				{
					camera.position = normalize(player->position) * cameraDistance;
					camera.target = { 0, 0, 0 }; // assumes center of level is origin
					
					Plane cameraPlane = plane(camera.position, southPole, northPole);
					camRight = camera.flipTest() * cameraPlane.normal;
				}
				else if(player->movementMode == NORMAL)
				{
					camera.position = normalize(project(player->position, poleCameraPlane)) * cameraDistance;
					camera.target = { 0, 0, 0 }; // assumes center of level is origin
					
					camRight = camera.right();
				}
				else
				{
					ALS_ILLEGAL;
				}

				Vec3 newCameraUp = normalize(cross(camRight, -camera.position));
				camera.orientation = lookRotation(
					camera.target - camera.position,
					newCameraUp
				);

				if(camera.up().y <= 0)
				{
					auto zoop = 0;
				}

				ALS_ASSERT(isOrthogonal(camera.forward(), camera.up()));
			}
			
			scene.view = camera.viewMatrix();
			// scene.view = realCam.viewMatrix(); // DEBUG

			forward = camera.up();
			right = camera.right();
			
			if(player->movementMode == NORMAL)
			{
				forward = project(forward, standingPlane);
				right = project(right, standingPlane);
			}

			Vec3 newFacing = (right * player->velocity.right) + (forward * player->velocity.forward);
			Vec3 newUp = normalize(player->position);
			player->orientation = lookRotation(newFacing, newUp);
		}

		if(player->movementMode == ORBITAL && (player->bottomPoint().y > northSliceY || player->bottomPoint().y < southSliceY))
		{
			player->movementMode = NORMAL;

			if(camera.isNorthUp())
			{
				poleCameraPlane = plane(camera.position, southPole, northPole);
				poleFlipPlane = plane(northPole, southPole, northPole + poleCameraPlane.normal);
			}
			else
			{
				poleCameraPlane = plane(camera.position, northPole, southPole);
				poleFlipPlane = plane(southPole, northPole, southPole + poleCameraPlane.normal);
			}
		}
		else if (player->movementMode == NORMAL && player->bottomPoint().y <= northSliceY && player->bottomPoint().y >= southSliceY)
		{
			player->movementMode = ORBITAL;
		}
		
		// APPLY FRICTION
		if(!isZeroVector(player->velocity.asVector2))
		{
			player->velocity.asVector2 *= (1 - globals.physics.friction);

			if(lengthSquared(player->velocity.asVector2) < globals.physics.minVelocity)
			{
				player->velocity.asVector2 = zeroVec2();
			}
		}

		scene.lights[0].position.x = 8;
		scene.lights[0].position.y = 2;
		scene.lights[0].position.z = 8;

		// DRAW LEVEL
		{
			drawSphere(
				&scene,
				level->position,
				level->radius(),
				vector3(.6, .6, .6));
		}

		
		// DRAW CYLINDERS AT LATITUDES
		{
			for(int i = 0; i < level->sliceCount - 1; i++)
			{
				real32 y = level->slices[i].topY;
				real32 latitude = TO_DEG(asin(y / level->radius()));
				real32 latitudeRadius = level->radius() * abs(cos(TO_RAD(latitude)));

				Quaternion unchanged = identityQuaternion();
				
				drawCylinder(
					&scene,
					y * vector3(0, 1, 0),
					unchanged,
					latitudeRadius + 0.1,
					.1,
					vector3(0, 0, 0));
			}
		}

		// DRAW POLES
		{
			Quaternion q = identityQuaternion();
			
			// NORTH
			drawRectPrism(&scene,
						  northPole,
						  q,
						  {.15f, 4, .15f},
						  { 1, 0, 0});

			// SOUTH
			drawRectPrism(&scene,
						  southPole,
						  q,
						  {.15f, 4, .15f},
						  { 1, 1, 1});

			// EAST
			drawRectPrism(&scene,
						  vector3(1, 0, 0) * level->radius(),
						  q,
						  {4, .15f, .15f},
						  { 0, 1, 0});

			// WEST
			drawRectPrism(&scene,
						  vector3(-1, 0, 0) * level->radius(),
						  q,
						  {4, .15f, .15f},
						  { 1, 1, 1});

			// FRONT
			drawRectPrism(&scene,
						  vector3(0, 0, 1) * level->radius(),
						  q,
						  {.15f, .15f, 4},
						  { 0, 0, 1});

			// BACK
			drawRectPrism(&scene,
						  vector3(0, 0, -1) * level->radius(),
						  q,
						  {.15f, .15f, 4},
						  { 1, 1, 1});
		}

		// DRAW PLAYER
		{
			drawCylinder(
				&scene,
				player->centerPoint(),
				player->orientation,
				player->size.x / 2,
				player->size.z,
				vector3(0, 0, 1));

			// DEBUG: draw circle at bottom point of player!
			// drawCircle(&scene,
			// 		   player->bottomPoint(),
			// 		   upVector(player->orientation),
			// 		   globals.sizes.udepiRadius * 1.2,
			// 		   vector3(1, 0, 0)
			// 	);
		}

		// DRAW LIGHTS
		{
			drawLights(&scene);
		}

		// DRAW WALL
		{
			drawWall(&scene, wall);
			Vec3 uv = upVector(wall->orientation);

			drawQuad(
				&scene,
				vector3( 3, 3, 0 ),
				vector3( 5, 3, 0 ),
				vector3( 5, 5, 0 ),
				vector3( 3, 5, 0 ),
				vector3( 1, 1, 1)
			);
			
			drawArrow(&scene,
					  wall->position,
					  wall->position + 8* vector3(-.627963, 0.45970, -.627963),
					  2,
					  vector4(1, 1, 1, 1));
			
			drawArrow(&scene,
					  wall->position,
					  wall->position + 8* vector3(-.211324811, -0.57735, .78867),
					  2,
					  vector4(1, 1, 1, 1));

			// drawRectPrism(
			// 	&scene,
			// 	vector3(0, 0, 10),
			// 	identityQuaternion(),
			// 	vector3(1, 1, 1),
			// 	vector3(1, 1, 1));
		}

		// Print Player Position and relative velocity
		{
			char output[512];

			// Pos
			snprintf(
				output,
				ALS_ARRAY_LEN(output),
				"X: %.3f\nY: %.3f\nZ: %.3f",
				player->position.x,
				player->position.y,
				player->position.z
			);
			
			drawText(
				&scene,
				vector2(20, 20 + scene.font->capsHeight),
				vector2(windowWidth, windowHeight),
				output,
				vector4(0, 0, 0, 1)
			);

			// Rel Vel
			snprintf(
				output,
				ALS_ARRAY_LEN(output),
				"Relative Vel.\nRight: %.3f\nForward: %.3f\nUp: %.3f",
				player->velocity.right,
				player->velocity.forward,
				player->velocity.up
			);
			
			drawText(
				&scene,
				vector2(windowWidth - 300, 20 + scene.font->capsHeight),
				vector2(windowWidth, windowHeight),
				output,
				vector4(0, 0, 0, 1)
			);
		}

		// Print avg frame rendering time every 16 frames
		{
			real32 endFrame = glfwGetTime();
			real32 timeDiff = endFrame - beginFrame;
		
			beginFrame = endFrame;
			recentFrames[recentFrameIndex] = timeDiff;
				
			frameCounter++;
			recentFrameIndex++;
			recentFrameIndex &= 0xF;

			ALS_ASSERT(recentFrameIndex < 16);

			real32 avgFrameTime = 0;
			int foo = ALS_ARRAY_LEN(recentFrames);
			for(int i = 0; i < ALS_ARRAY_LEN(recentFrames); i++)
			{
				avgFrameTime += recentFrames[i];
			}
			avgFrameTime /= ALS_ARRAY_LEN(recentFrames);

			char output[512];
			snprintf(
				output,
				ALS_ARRAY_LEN(output),
				"Frame time: %.4f",
				avgFrameTime
			);

			drawText(
				&scene,
				vector2(20, windowHeight - 20),
				vector2(windowWidth, windowHeight),
				output,
				vector4(0, 0, 0, 1)
			);
		}

		for(int i = 0; i < ALS_ARRAY_LEN(keys); i++)
		{
			lastKeys[i] = keys[i];
		}
		
		GLenum errorEnum = glGetError();
		ALS_ASSERT(errorEnum == GL_NO_ERROR);
		
		glfwSwapBuffers(window);

		errorEnum = glGetError();
		ALS_ASSERT(errorEnum == GL_NO_ERROR);
	}

	glfwTerminate();
	return 0;
}
