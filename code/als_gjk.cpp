Vec3 epaPenetrationVector(Entity* a, Entity* b, GjkSimplex gjkSimplex);
	
//
// GJK
//
Vec3 gjkSupport(Entity* entity, Vec3 direction)
{
	Vec3 result;
	direction = normalize(direction);

	Vec3 offset; // From center point
	
	switch(entity->colliderType)
	{
		case SPHERE:
		{
			SphereCollider* collider = (SphereCollider*)entity->collider;
			
			offset = collider->radius * direction;
		} break;

		case CYLINDER:
		{
			// Support point will always be on the perimeter of one of the circular faces
			CylinderCollider* collider = (CylinderCollider*)entity->collider;
			
			Vec3 up = vector3(0, 1, 0);
			up = entity->orientation * up;

			Vec3 directionNorm = normalize(direction);

			if(equals(up, directionNorm))
			{
				offset = up * (collider->height / 2);
			}
			else if (equals(up, directionNorm))
			{
				offset = -up * (collider->height / 2);
			}
			else
			{
				// Direction isn't parallel to up, so we can project it onto
				// one of the cylinder's circular faces
				Vec3 dirToCircle = up;
				
				if(dot(up, direction) > 0)
				{
					dirToCircle = up;
				}
				else
				{
					dirToCircle = -up;
				}
				
				Plane circle = plane(entity->centerPoint() + dirToCircle * collider->height / 2, up);;
				Vec3 directionProjected = normalize(project(direction, circle));
					
				offset = dirToCircle * (collider->height / 2);
				offset += directionProjected * collider->radius;
			}
		} break;

		case RECT3:
		{
			// Support point will always be one of the 8 vertices
			BoxCollider* collider = (BoxCollider*)entity->collider;

			Vec3 right = vector3(1, 0, 0);
			Vec3 up = vector3(0, 1, 0);
			Vec3 forward = vector3(0, 0, 1);
			
			// vectors from the entity's center to the front/right/top faces
			Vec3 toRight = entity->orientation * (collider->dimensions.x / 2 * right);
			Vec3 toTop = entity->orientation * (collider->dimensions.y / 2 * up);
			Vec3 toFront = entity->orientation * (collider->dimensions.z / 2 * forward);

			// OPTIMIZATION: 
			real32 rightSign = (dot(toRight, direction) >= 0.0f)  ?  1  :  -1;
			real32 topSign   = (dot(toTop, direction)   >= 0.0f)  ?  1  :  -1;
			real32 frontSign = (dot(toFront, direction) >= 0.0f)  ?  1  :  -1;

			offset = (rightSign * toRight)   +   (topSign * toTop)   +   (frontSign * toFront);
		} break;

		default:
		{
			ALS_ILLEGAL;
		}
	}

	result = entity->centerPoint() + offset;
	return result;
}

// Support of the Minkowski difference of A and B.
Vec3 gjkSupport(Entity* a, Entity* b, Vec3 direction)
{
	// OPTIMIZE: can we "fudge" this since in certain cases since we
	// have special knowledge of the shapes? Casey Muratori says we can...
	// https://www.youtube.com/watch?v=Qupqu1xe7Io&t=12m23s

	Vec3 result = gjkSupport(a, direction) - gjkSupport(b, -direction);
	return result;
}

bool32 gjkDoSimplex(GjkSimplex* simplex, Vec3* direction)
{
	// Great explanation here
    // https://www.youtube.com/watch?v=Qupqu1xe7Io
	switch(simplex->count)
	{
		case 2:
		{
			// a = point we just added
			// b = previous point in simplex
			// o = origin

			Vec3 a = simplex->points[1];
			Vec3 b = simplex->points[0];
			
			Vec3 ab = b - a;
			Vec3 ao = -a;

			if(dot(ab, ao) > 0.0f)
			{
				*direction = cross(cross(ab, ao), ab);
			}
			else
			{
				*direction = ao;
			}

			return false;
		} break;

		case 3:
		{
			// a = point we just added
			// b = 2nd newest point in simplex
			// c = first point in simplex
			// o = origin
			Vec3 a = simplex->points[2];
			Vec3 b = simplex->points[1];
			Vec3 c = simplex->points[0];
			
			Vec3 ab = b - a;
			Vec3 ac = c - a;
			Vec3 ao = -a;
			Vec3 triNormal = cross(ab, ac);

			if(dot(cross(triNormal, ac), ao) > 0.0f)
			{
				if(dot(ac, ao) > 0.0f)
				{
					Vec3 newSimplex[2] = { a, c };
					simplex->reconstruct(newSimplex, 2);
					*direction = cross(cross(ac, ao), ac);
				}
				else
				{
				starCase:
					{
						if(dot(ab, ao) > 0.0f)
						{
							Vec3 newSimplex[2] = { a, b };
							simplex->reconstruct(newSimplex, 2);
							*direction = cross(cross(ab, ao), ab);
						}
						else
						{
							simplex->reconstruct(&a, 1);
							*direction = ao;
						}
					}
				}
			}
			else
			{
				if(dot(cross(ab, triNormal), ao) > 0.0f)
				{
					goto starCase; // PLEASE DON'T KILL ME
				}
				else
				{
					// inside triangle... but which side?
					if(dot(triNormal, ao) > 0.0f)
					{
						// Can we make this better? Instead of searching along the normal, can we search the direction from
						// the center point of the triangle to the origin? Would this be better? I think so...
						Vec3 newSimplex[3] = { b, c, a };
						simplex->reconstruct(newSimplex, 3);
						
						*direction = triNormal;
					}
					else
					{
						// Re-wind triangle (so our tetrahedron simplex can be consistent about knowing which way is CCW)
						// and search in the opposite direction!
						*direction = -triNormal;
					}
				}
			}
		} break;

		case 4:
		{
			Vec3 a = simplex->points[3];
			Vec3 b = simplex->points[2];
			Vec3 c = simplex->points[1];
			Vec3 d = simplex->points[0];
			
			Vec3 ab = b - a;
			Vec3 ac = c - a;
			Vec3 ad = d - a;
			Vec3 ao = -a;
			
			Vec3 acbNormal = cross(ac, ab);
			Vec3 adcNormal = cross(ad, ac);
			Vec3 abdNormal = cross(ab, ad);

			if(dot(acbNormal, ao) > 0.0f)
			{
				Vec3 newSimplex[3] = { a, c, b };
				simplex->reconstruct(newSimplex, 3);
				*direction = acbNormal;

				gjkDoSimplex(simplex, direction);
			}
			else if(dot(adcNormal, ao) > 0.0f)
			{
				Vec3 newSimplex[3] = { a, d, c };
				simplex->reconstruct(newSimplex, 3);
				*direction = adcNormal;

				gjkDoSimplex(simplex, direction);
			}
			else if(dot(abdNormal, ao) > 0.0f)
			{
				Vec3 newSimplex[3] = { a, b, d };
				simplex->reconstruct(newSimplex, 3);
				*direction = abdNormal;

				gjkDoSimplex(simplex, direction);
			}
			else
			{
				return true; // yay!!!!!!!!!
			}
		} break;

		default:
		{
			ALS_ILLEGAL;
		} break;
	}

	return false;
}

bool32 gjkCollisionTest(Entity* a, Entity* b, GjkResult* result)
{
	bool32 collides = false;
	
	if(result)
	{
		result->collides = false;
		result->penetration = vector3(0, 0, 0);
	}
	
	GjkSimplex simplex;
	Vec3 direction = vector3(1, 0, 0);
	
	// Seed the simplex with any point in the Minkowski difference
	simplex.add(gjkSupport(a, b, direction));

	// Origin (by definition) is in the opposite direction of the seed point!
	if(isZeroVector(simplex.points[0]))
	{
		result->collides = false; // TODO: is this right?
		result->penetration = vector3(0, 0, 0);

		return false;
	}
	
	direction = -simplex.points[0];

	// TODO: investigate what the max iteration count should be
	for(int32 iteration = 0; iteration < 20; iteration++)
	{
		Vec3 furthestPoint = gjkSupport(a, b, direction);

		// Furthest point in direction of origin doesn't pass the origin...
		// No intersection!
		if(dot(furthestPoint, direction) < 0.0f)
		{
			// NO COLLISION
			break;
		}
		else
		{
			simplex.add(furthestPoint);
			
			if(gjkDoSimplex(&simplex, &direction))
			{
				// COLLISION
				if(result)
				{
					result->collides = true;
					result->penetration = epaPenetrationVector(a, b, simplex);
				}

				collides = true;
				break;
			}
		}
	}

	return collides;
}


//
// EPA
//
Vec3 epaPenetrationVector(Entity* a, Entity* b, GjkSimplex gjkSimplex)
{
	EpaSimplex simplex;
	simplex.initFromGjkSimplex(gjkSimplex);

	Vec3 result;
	
	while(true)
	{
		Triangle face;
		uint32 faceIndex;
		real32 distance = simplex.closestFaceToOrigin(&face, &faceIndex);
		
		Vec3 faceNormal = face.normal();
		Vec3 furthestInNormal = gjkSupport(a, b, faceNormal);
		real32 furthestInNormalDistance = dot(furthestInNormal, faceNormal);

		// SOME tolerance for things like spheres/cylinders which can almost always get a LIIIIITLE closer
		if(furthestInNormalDistance > distance + 0.01)
		{
			simplex.extend(furthestInNormal);
		}
		else
		{
			// Simplex can't be expanded, so we have the closest face!
			// Calculate distance to plane
			Plane p = plane(face.a, face.normal());

			real32 d = sqrt(distanceSquared(vector3(0, 0, 0), p));
			result = face.normal() * d;

			break;
		}
	}
	
	return result;
}
