struct GlobalPhysics
{
	real32 friction = 0.2; // range 0 - 1
	real32 minVelocity = .0001; // anything less than this gets set to 0
	real32 gravity = -9.8;
};

struct GlobalSizes
{
	real32 udepiRadius = 1.5f;
	real32 udepiHeight = 2.0f;
};

struct GlobalCapacities
{
	enum maxLevelSliceCount { maxLevelSliceCount = 64 };
};

struct Globals
{
	GlobalPhysics physics;
	GlobalSizes sizes;
	GlobalCapacities capacities;
};
