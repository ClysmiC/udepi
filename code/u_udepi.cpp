enum MovementMode
{
	ORBITAL, // Moving left/right follows a latitude line, effectively causing you to "orbit"
	NORMAL,  // Moving left/right moves directly left/right relative to the camera. This is only used at poles.
};

struct Udepi : Entity
{
	real32 accel;

	real32 maxVelocity; // probably impossible to reach due to friction...
	real32 maxVelocitySquared;

	real32 jumpVelocity;
	
	RelativeVector velocity;

	Vec3 size; // stored here for convenience

	MovementMode movementMode;
};

Udepi* constructUdepi(Vec3 position, Quaternion orientation)
{
	Udepi* udepi;
	ALS_ALLOC(Udepi, udepi, 1);
	
	udepi->velocity = {};
	udepi->movementMode = ORBITAL;

	udepi->colliderType = CYLINDER;
	udepi->collider = constructCylinderCollider(
		globals.sizes.udepiRadius,
		globals.sizes.udepiHeight,
		udepi);

	udepi->flags = DYNAMIC_MOVE | POSITION_AT_BOTTOM;

	udepi->accel = 3.5f;
	udepi->maxVelocity = 6.0f;
	udepi->maxVelocitySquared = udepi->maxVelocity * udepi->maxVelocity;

	udepi->jumpVelocity = 4;

	udepi->size.x = globals.sizes.udepiRadius;
	udepi->size.y = globals.sizes.udepiHeight;
	udepi->size.z = globals.sizes.udepiRadius;
	
	udepi->position = position;
	udepi->orientation = orientation;

	return udepi;
}
