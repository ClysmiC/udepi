void _initFontTexture(Scene* scene)
{
	uint32 glError;

	real32 fontHeight = 32.0;
	
	// INIT FONT STUFF
	{
		ALS_ALLOC(Font, scene->font, 1);
		scene->font->glTextureDimension = 512;
		
		const uint32 ttf_buffer_size = 1 << 20;
		unsigned char* ttf_file_buffer;

		unsigned char* ttf_bitmap;
		
		ALS_ALLOC(unsigned char, ttf_file_buffer, ttf_buffer_size);
		ALS_ALLOC(unsigned char, ttf_bitmap, scene->font->glTextureDimension * scene->font->glTextureDimension);

		// Copy ttf file into a buffer in memory
		uint32 result = fread(ttf_file_buffer, 1, ttf_buffer_size, fopen("C:/Windows/Fonts/arial.ttf", "rb"));
		if(result <= 0)
		{
			ALS_ILLEGAL;
		}

		// Generates a bitmap that will become a texture containing each glyph.
		// Also returns the offsets into the texture for each character in the
		// "characterData" array
		stbtt_BakeFontBitmap(ttf_file_buffer, 0,
							 fontHeight,
							 ttf_bitmap,
							 scene->font->glTextureDimension, scene->font->glTextureDimension,
							 32, 96, scene->font->characterData);

		// Calculate "caps height"
		{
			stbtt_aligned_quad q;
			real32 x, y;
			stbtt_GetBakedQuad(
				scene->font->characterData,
				scene->font->glTextureDimension, scene->font->glTextureDimension,
				'A' - 32,
				&x, &y,
				&q, 1
			);

			scene->font->capsHeight = q.y1 - q.y0;
			scene->font->lineHeight = fontHeight;
		}

		glGenTextures(1, &scene->font->glTexture);
		
		glBindTexture(GL_TEXTURE_2D, scene->font->glTexture);
		{
			glError = glGetError();
			ALS_ASSERT(glError == GL_NO_ERROR);
			
			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_R8,
				scene->font->glTextureDimension, scene->font->glTextureDimension, 0,
				GL_RED,
				GL_UNSIGNED_BYTE,
				ttf_bitmap
			);

			glError = glGetError();
			ALS_ASSERT(glError == GL_NO_ERROR);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

			glError = glGetError();
			ALS_ASSERT(glError == GL_NO_ERROR);
		}
		glBindTexture(GL_TEXTURE_2D, 0);

		free(ttf_file_buffer);
		free(ttf_bitmap);
	}
}

void _initSphere(CommonGraphicsShapes* shapes)
{
	GLuint sphereVBO;
	glGenBuffers(1, &sphereVBO);
	glGenVertexArrays(1, &shapes->sphere.vao);

	const int subdivisions = 5;

	int totalSphereTriangles = 8 * powi(4, 5);
	// for(int i = 0; i < subdivisions; i++) {
	// 	totalSphereTriangles *= 4;
	// }
	
	GfxTriangle *unitSphereTriangles = (GfxTriangle*)malloc(totalSphereTriangles * sizeof(GfxTriangle));

	shapes->sphere.numTriangles = totalSphereTriangles;

	// Step 1: construct octahedron (should be an equal number of '4's
	// to the number of subdivisions). Could replace array with vector
	// and use dynamic amount of subdivisions.

	// Note: Triangles are stored counter-clockwise

	// top-back-right
	unitSphereTriangles[0].a = {0, 1, 0};
	unitSphereTriangles[0].b = {1, 0, 0};
	unitSphereTriangles[0].c = {0, 0, -1};

	// top-front-right
	unitSphereTriangles[1].a = {0, 1, 0};
	unitSphereTriangles[1].b = {0, 0, 1};
	unitSphereTriangles[1].c = {1, 0, 0};

	// top-front-left
	unitSphereTriangles[2].a = {0, 1, 0};
	unitSphereTriangles[2].b = {-1, 0, 0};
	unitSphereTriangles[2].c = {0, 0, 1};

	// top-back-left
	unitSphereTriangles[3].a = {0, 1, 0};
	unitSphereTriangles[3].b = {0, 0, -1};
	unitSphereTriangles[3].c = {-1, 0, 0};

	// bot-back-right
	unitSphereTriangles[4].a = {0, -1, 0};
	unitSphereTriangles[4].b = {0, 0, -1};
	unitSphereTriangles[4].c = {1, 0, 0};

	// bot-front-right
	unitSphereTriangles[5].a = {0, -1, 0};
	unitSphereTriangles[5].b = {1, 0, 0};
	unitSphereTriangles[5].c = {0, 0, 1};

	// bot-front-left
	unitSphereTriangles[6].a = {0, -1, 0};
	unitSphereTriangles[6].b = {0, 0, 1};
	unitSphereTriangles[6].c = {-1, 0, 0};

	// bot-back-left
	unitSphereTriangles[7].a = {0, -1, 0};
	unitSphereTriangles[7].b = {-1, 0, 0};
	unitSphereTriangles[7].c = {0, 0, -1};

	// Subdivide the existing triangles 5 times
	for(int currentSubdivision = 0; currentSubdivision < subdivisions; currentSubdivision++)
	{
		int trianglesPreDivide = 8 * powi(4, currentSubdivision);
		int trianglesPostDivide = trianglesPreDivide * 4;

		int newTriangleIndex = trianglesPostDivide - 1;

		// Subdivide from end to beginning and place the new from back
		// to front, so that by the time the "before" triangles start
		// getting overwritten, they wil have already been subdivided.
		for(int i = trianglesPreDivide - 1; i >= 0; i--)
		{
			GfxTriangle t = unitSphereTriangles[i];

			Vec3 d = (t.a + t.b) / 2;
			Vec3 e = (t.b + t.c) / 2;
			Vec3 f = (t.a + t.c) / 2;

			GfxTriangle t1 = { t.a, d, f };
			GfxTriangle t2 = { t.b, e, d };
			GfxTriangle t3 = { t.c, f, e };
			GfxTriangle t4 = { d, e, f };

			ALS_ASSERT(t1.a.x >= -1 && t1.a.x <= 1);
			ALS_ASSERT(t1.a.y >= -1 && t1.a.y <= 1);
			ALS_ASSERT(t1.a.z >= -1 && t1.a.z <= 1);

			ALS_ASSERT(t2.a.x >= -1 && t2.a.x <= 1);
			ALS_ASSERT(t2.a.y >= -1 && t2.a.y <= 1);
			ALS_ASSERT(t2.a.z >= -1 && t2.a.z <= 1);

			ALS_ASSERT(t3.a.x >= -1 && t3.a.x <= 1);
			ALS_ASSERT(t3.a.y >= -1 && t3.a.y <= 1);
			ALS_ASSERT(t3.a.z >= -1 && t3.a.z <= 1);

			ALS_ASSERT(t4.a.x >= -1 && t4.a.x <= 1);
			ALS_ASSERT(t4.a.y >= -1 && t4.a.y <= 1);
			ALS_ASSERT(t4.a.z >= -1 && t4.a.z <= 1);

			unitSphereTriangles[newTriangleIndex] = t1;
			newTriangleIndex--;
			unitSphereTriangles[newTriangleIndex] = t2;
			newTriangleIndex--;
			unitSphereTriangles[newTriangleIndex] = t3;
			newTriangleIndex--;
			unitSphereTriangles[newTriangleIndex] = t4;
			newTriangleIndex--;
		}
	}

	// Note: 6 attributes per vertex, 3 vertices per triangle
	int32 totalFloats = totalSphereTriangles * 6 * 3;
	
	// Set up array to attach to VBO
	GLfloat* vertices;
	ALS_ALLOC(GLfloat, vertices, totalFloats);
	
	for(int i = 0; i < totalSphereTriangles; i++)
	{
		GfxTriangle *t = &unitSphereTriangles[i];

		for(int point = 0; point < 3; point++)
		{
			Vec3* p = &t->points[point];
			*p = normalize(*p);

			int32 index = i * 18 + point * 6;

			vertices[index++] = p->x;
			vertices[index++] = p->y;
			vertices[index++] = p->z;

			vertices[index++] = p->x;
			vertices[index++] = p->y;
			vertices[index] = p->z;

			ALS_ASSERT(index < totalFloats);
		}
	}

	glBindVertexArray(shapes->sphere.vao);
	{
		glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
		glBufferData(GL_ARRAY_BUFFER, totalFloats * sizeof(GLfloat), vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
	}
	glBindVertexArray(0);

	glDeleteBuffers(1, &sphereVBO);

	// Data is now stored in VAOs -- can be freed on heap
	free(vertices);
	free(unitSphereTriangles);

	GLenum errorEnum = glGetError();
	ALS_ASSERT(errorEnum == GL_NO_ERROR);
}

void _initCylinder(CommonGraphicsShapes* shapes)
{
	GLuint cylinderVBO;
	glGenBuffers(1, &cylinderVBO);
	glGenVertexArrays(1, &shapes->cylinder.vao);

	// Note: high number of sides simulates a circle
	const uint32 numSides = 32;
		
	// num sides triangles in a "fan" for top and bottom...
	// And each "side" is a rectangle, i.e., two triangles
	const uint32 totalTriangles = numSides * 4;

	shapes->cylinder.numTriangles = totalTriangles;
	GfxTriangle triangles[totalTriangles];

	real32 prevAngle = 0.0f;
		
	for(int i = 0; i < numSides; i++)
	{
		real32 angle = (360.0f / numSides) * (i + 1);

		// Note: remember +z is "down" if looking at the XZ plane from above
		// NOTE: READ THIS FUCKING NOTE BEFORE WASTING AN HOUR ON THIS EVEN
		// THOUGH I ALREADY WROTE A NOTE ABOUT IT GOD DAMMIT
		real32 x1 = 0.0f;
		real32 z1 = 0.0f;
		real32 x2 = cos(TO_RAD(angle));
		real32 z2 = sin(TO_RAD(angle));
		real32 x3 = cos(TO_RAD(prevAngle));
		real32 z3 = sin(TO_RAD(prevAngle));

		// Top face
		triangles[i].a = { x1, 0.5f, z1 };
		triangles[i].b = { x2, 0.5f, z2 };
		triangles[i].c = { x3, 0.5f, z3 };
		triangles[i].aNormal = { 0.0f, 1.0f, 0.0f };
		triangles[i].bNormal = { 0.0f, 1.0f, 0.0f };
		triangles[i].cNormal = { 0.0f, 1.0f, 0.0f };

		// Bot face (note c and b are swapped to keep CCW winding)
		triangles[numSides + i].a = { x1, -0.5f, z1 };
		triangles[numSides + i].b = { x3, -0.5f, z3 };
		triangles[numSides + i].c = { x2, -0.5f, z2 };
		triangles[numSides + i].aNormal = { 0.0f, -1.0f, 0.0f };
		triangles[numSides + i].bNormal = { 0.0f, -1.0f, 0.0f };
		triangles[numSides + i].cNormal = { 0.0f, -1.0f, 0.0f };

		// Side triangle 1
		triangles[numSides * 2 + i].a = { x2, 0.5f, z2 };
		triangles[numSides * 2 + i].aNormal = normalize(vector3(x2, 0.0f, z2));
			
		triangles[numSides * 2 + i].b = { x2, -0.5f, z2 };
		triangles[numSides * 2 + i].bNormal = normalize(vector3(x2, 0.0f, z2));
			
		triangles[numSides * 2 + i].c = { x3, 0.5f, z3 };
		triangles[numSides * 2 + i].cNormal = normalize(vector3(x3, 0.0f, z3));

		// Side triangle 2
		triangles[numSides * 3 + i].a = { x2, -0.5f, z2 };
		triangles[numSides * 3 + i].aNormal = normalize(vector3(x2, 0.0f, z2));
			
		triangles[numSides * 3 + i].b = { x3, -0.5f, z3 };
		triangles[numSides * 3 + i].bNormal = normalize(vector3(x3, 0.0f, z3));
			
		triangles[numSides * 3 + i].c = { x3, 0.5f, z3 };
		triangles[numSides * 3 + i].cNormal = normalize(vector3(x3, 0.0f, z3));
			
		prevAngle = angle;
	}

	// Set up array to attach to VBO
	// 3 vertices per triangle, 6 data points per vertex (pos an normal)
	GLfloat vertices[totalTriangles * 3 * 6];
	
	for(int i = 0; i < totalTriangles; i++)
	{
		GfxTriangle *t = &triangles[i];

		for(int point = 0; point < 3; point++)
		{
			Vec3* p = &t->points[point];
			Vec3* n = &t->normals[point];

			vertices[i * 18 + point * 6 + 0] = p->x;
			vertices[i * 18 + point * 6 + 1] = p->y;
			vertices[i * 18 + point * 6 + 2] = p->z;
			vertices[i * 18 + point * 6 + 3] = n->x;
			vertices[i * 18 + point * 6 + 4] = n->y;
			vertices[i * 18 + point * 6 + 5] = n->z;
		}
	}
		

	glBindVertexArray(shapes->cylinder.vao);
	{
		glBindBuffer(GL_ARRAY_BUFFER, cylinderVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
	}
	glBindVertexArray(0);

	glDeleteBuffers(1, &cylinderVBO);
}

void _initCube(CommonGraphicsShapes* shapes)
{
	GLuint cubeVBO;
	glGenBuffers(1, &cubeVBO);
	glGenVertexArrays(1, &shapes->cube.vao);

	shapes->cube.numTriangles = 12;

	GLfloat vertices[] = {
		// Positions             // Normals
		-0.5f, -0.5f, -0.5f,     0.0f,  0.0f, -1.0f,
		0.5f,  0.5f, -0.5f,      0.0f,  0.0f, -1.0f, 
		0.5f, -0.5f, -0.5f,      0.0f,  0.0f, -1.0f, 
		0.5f,  0.5f, -0.5f,      0.0f,  0.0f, -1.0f, 
		-0.5f, -0.5f, -0.5f,     0.0f,  0.0f, -1.0f, 
		-0.5f,  0.5f, -0.5f,     0.0f,  0.0f, -1.0f, 

		-0.5f, -0.5f,  0.5f,     0.0f,  0.0f, 1.0f,
		0.5f, -0.5f,  0.5f,      0.0f,  0.0f, 1.0f,
		0.5f,  0.5f,  0.5f,      0.0f,  0.0f, 1.0f,
		0.5f,  0.5f,  0.5f,      0.0f,  0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,     0.0f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,     0.0f,  0.0f, 1.0f,

		-0.5f,  0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,

		0.5f,  0.5f,  0.5f,      1.0f,  0.0f,  0.0f,
		0.5f, -0.5f, -0.5f,      1.0f,  0.0f,  0.0f,
		0.5f,  0.5f, -0.5f,      1.0f,  0.0f,  0.0f,
		0.5f, -0.5f, -0.5f,      1.0f,  0.0f,  0.0f,
		0.5f,  0.5f,  0.5f,      1.0f,  0.0f,  0.0f,
		0.5f, -0.5f,  0.5f,      1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,     0.0f, -1.0f,  0.0f,
		0.5f, -0.5f, -0.5f,      0.0f, -1.0f,  0.0f,
		0.5f, -0.5f,  0.5f,      0.0f, -1.0f,  0.0f,
		0.5f, -0.5f,  0.5f,      0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,     0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,     0.0f, -1.0f,  0.0f,

		-0.5f,  0.5f, -0.5f,     0.0f,  1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,      0.0f,  1.0f,  0.0f,
		0.5f,  0.5f, -0.5f,      0.0f,  1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,      0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,     0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,     0.0f,  1.0f,  0.0f
	};

	glBindVertexArray(shapes->cube.vao);
	{
		glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
	}
	glBindVertexArray(0);

	glDeleteBuffers(1, &cubeVBO);
}

void _initCircle(CommonGraphicsShapes* shapes)
{
	GLuint circleVBO;
	glGenBuffers(1, &circleVBO);
	glGenVertexArrays(1, &shapes->circle.vao);

	// Note: high number of sides simulates a circle
	const uint32 numSides = 32;
		
	// One circle winding CW, one CCW
	const uint32 totalTriangles = numSides * 2;

	shapes->circle.numTriangles = totalTriangles;

	GfxTriangle triangles[totalTriangles];

	real32 prevAngle = 0.0f;
		
	// Top face and bottom face
	for(int i = 0; i < numSides; i++)
	{
		real32 angle = (360.0f / numSides) * (i + 1);

		// Note: remember z "down" if looking at the XZ plane from above
		real32 x1 = 0.0f;
		real32 z1 = 0.0f;
		real32 x2 = cos(TO_RAD(angle));
		real32 z2 = sin(TO_RAD(angle));
		real32 x3 = cos(TO_RAD(prevAngle));
		real32 z3 = sin(TO_RAD(prevAngle));

		// Top face
		triangles[i].a = { x1, 0.0f, z1 };
		triangles[i].b = { x2, 0.0f, z2 };
		triangles[i].c = { x3, 0.0f, z3 };
		triangles[i].aNormal = { 0.0f, 1.0f, 0.0f };
		triangles[i].bNormal = { 0.0f, 1.0f, 0.0f };
		triangles[i].cNormal = { 0.0f, 1.0f, 0.0f };

		// Bot face (note c and b are swapped to keep CCW winding)
		triangles[numSides + i].a = { x1, 0.0f, z1 };
		triangles[numSides + i].b = { x3, 0.0f, z3 };
		triangles[numSides + i].c = { x2, 0.0f, z2 };
		triangles[numSides + i].aNormal = { 0.0f, -1.0f, 0.0f };
		triangles[numSides + i].bNormal = { 0.0f, -1.0f, 0.0f };
		triangles[numSides + i].cNormal = { 0.0f, -1.0f, 0.0f };

		prevAngle = angle;
	}

	// Set up array to attach to VBO
	// 3 vertices per triangle, 6 data points per vertex (pos an normal)
	GLfloat vertices[totalTriangles * 3 * 6];
	
	for(int i = 0; i < totalTriangles; i++)
	{
		GfxTriangle *t = &triangles[i];

		for(int point = 0; point < 3; point++)
		{
			Vec3* p = &t->points[point];
			Vec3* n = &t->normals[point];

			vertices[i * 18 + point * 6 + 0] = p->x;
			vertices[i * 18 + point * 6 + 1] = p->y;
			vertices[i * 18 + point * 6 + 2] = p->z;
			vertices[i * 18 + point * 6 + 3] = n->x;
			vertices[i * 18 + point * 6 + 4] = n->y;
			vertices[i * 18 + point * 6 + 5] = n->z;
		}
	}

	glBindVertexArray(shapes->circle.vao);
	{
		glBindBuffer(GL_ARRAY_BUFFER, circleVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
	}
	glBindVertexArray(0);

	glDeleteBuffers(1, &circleVBO);
}

void initCommonShapes(CommonGraphicsShapes* shapes)
{
	_initSphere(shapes);
	_initCylinder(shapes);
	_initCube(shapes);

	_initCircle(shapes);
}

void deleteCommonShapes(CommonGraphicsShapes* shapes)
{
	glDeleteVertexArrays(1, &shapes->sphere.vao);
	glDeleteVertexArrays(1, &shapes->cube.vao);
	glDeleteVertexArrays(1, &shapes->cylinder.vao);
	glDeleteVertexArrays(1, &shapes->circle.vao);
}

void drawLights(Scene* scene)
{
	// This isn't working...............
	// TODO: fix!

	
	// glUseProgram(lightShader);

	// glBindVertexArray(scene->commonShapes.cube.vao);
	// {
	// 	// Transformation Matrices

	// 	glUniformMatrix4fv(
	// 		glGetUniformLocation(lightShader, "view"),
	// 		1, GL_FALSE, glm::value_ptr(scene->view));

	// 	glUniformMatrix4fv(
	// 		glGetUniformLocation(lightShader, "projection"),
	// 		1, GL_FALSE, glm::value_ptr(scene->projection));
			

	// 	// TODO: support drawing multiple lights
	// 	for(int i = 0; i < scene->lightCount; i++)
	// 	{
	// 		Light* light = &scene->lights[i];

	// 		glm::mat4 model;
	// 		model = glm::translate(
	// 			model,
	// 			glm::vec3(
	// 				light->position.x,
	// 				light->position.y,
	// 				light->position.z));

	// 		int32 modelLoc = glGetUniformLocation(lightShader, "model");
	// 		ALS_ASSERT(modelLoc != -1);
	// 		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	// 		int32 lightColorLoc = glGetUniformLocation(lightShader, "lightColor");
	// 		ALS_ASSERT(lightColorLoc != -1);
			
	// 		glUniform3f(lightColorLoc, light->specular.r, light->specular.g, light->specular.b);

	// 	}

	// 	glDrawArrays(GL_TRIANGLES, 0, scene->commonShapes.cube.numTriangles);
	// }
	// glBindVertexArray(0);
}

void drawTriangle(Scene* scene, Vec3 a, Vec3 b, Vec3 c, Vec3 color)
{
	Vec3 aToB = b - a;
	Vec3 aToC = c - a;
	Vec3 normal = normalize(cross(aToB, aToC));
	
	GLfloat vertices[] = {
		a.x, a.y, a.z,    normal.x, normal.y, normal.z,
		b.x, b.y, b.z,    normal.x, normal.y, normal.z,
		c.x, c.y, c.z,    normal.x, normal.y, normal.z
	};

	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	
	GLuint VBO;
	glGenBuffers(1, &VBO);
	
	glUseProgram(basicShader);
	GLint currentShader;
	glGetIntegerv(GL_CURRENT_PROGRAM, &currentShader);

	glBindVertexArray(VAO);
	{
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		// Position
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);

		// Normal
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		// Transformation Matrices
		Mat4 model;
		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "model"),
			1, GL_TRUE, model.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "view"),
			1, GL_TRUE, scene->view.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "projection"),
			1, GL_TRUE, scene->projection.dataPointer());


		// Material (use a basic default material)
		real32 aTerm = 1.0f;
		real32 dTerm = 1.0f;
		real32 sTerm = 1.0f;
		real32 shininess = 8.0f;
			
		glUniform3f(
			glGetUniformLocation(currentShader, "material.ambient"),
			aTerm * color.r, aTerm * color.g, aTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.diffuse"),
			dTerm * color.r, dTerm * color.g, dTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.specular"),
			sTerm * color.r, sTerm * color.g, sTerm * color.b);

		glUniform1f(
			glGetUniformLocation(currentShader, "material.shininess"),
			shininess);

		glUniform3f(
			glGetUniformLocation(currentShader, "viewPos"),
			scene->camera->position.x, scene->camera->position.y, scene->camera->position.z);

		for(int i = 0; i < scene->lightCount; i++)
		{
			Light* light = &scene->lights[i];
			
			// TODO: actually store several lights in the shader instead
			// of just overwriting the single light
			glUniform3f(
				glGetUniformLocation(currentShader, "light.position"),
				light->position.x, light->position.y, light->position.z);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.ambient"),
				light->ambient.r, light->ambient.g, light->ambient.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.diffuse"),
				light->diffuse.r, light->diffuse.g, light->diffuse.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.specular"),
				light->specular.r, light->specular.g, light->specular.b);
		}

		glDrawArrays(GL_TRIANGLES, 0, 3);
	}
	glBindVertexArray(0);

	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &VAO);
}

void drawQuad(Scene* scene, Vec3 a, Vec3 b, Vec3 c, Vec3 d, Vec3 color)
{
	drawTriangle(scene, a, b, c, color);
	drawTriangle(scene, a, c, d, color);
}

void drawSphere(Scene* scene, Vec3 position, real32 radius, Vec3 color)
{
	if(radius <= 0.0f)
	{
		return;
	}

	// GLint currentShader = sphereShader;
	GLint currentShader = basicShader;
	glUseProgram(currentShader);

	GLenum errorEnum = glGetError();
	ALS_ASSERT(errorEnum == GL_NO_ERROR);
	
	glBindVertexArray(scene->commonShapes.sphere.vao);
	{
		Mat4 translate = translationMatrix(position);
		Mat4 scale = scalingMatrix(vector3(radius, radius, radius));
		
		Mat4 model = translate * scale;
		
		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "model"),
			1, GL_TRUE, model.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "view"),
			1, GL_TRUE, scene->view.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "projection"),
			1, GL_TRUE, scene->projection.dataPointer());

		// Material (use a basic default material)
		real32 aTerm = 1.0f;
		real32 dTerm = 1.0f;
		real32 sTerm = 1.0f;
		real32 shininess = 8.0f;

		GLenum errorEnum = glGetError();
		ALS_ASSERT(errorEnum == GL_NO_ERROR);
			
		glUniform3f(
			glGetUniformLocation(currentShader, "material.ambient"),
			aTerm * color.r, aTerm * color.g, aTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.diffuse"),
			dTerm * color.r, dTerm * color.g, dTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.specular"),
			sTerm * color.r, sTerm * color.g, sTerm * color.b);

		glUniform1f(
			glGetUniformLocation(currentShader, "material.shininess"),
			shininess);

		glUniform3f(
			glGetUniformLocation(currentShader, "viewPos"),
			scene->camera->position.x, scene->camera->position.y, scene->camera->position.z);

		errorEnum = glGetError();
		ALS_ASSERT(errorEnum == GL_NO_ERROR);

		for(int i = 0; i < scene->lightCount; i++)
		{
			Light* light = &scene->lights[i];
			
			// TODO: actually store several lights in the shader instead
			// of just overwriting the single light
			glUniform3f(
				glGetUniformLocation(currentShader, "light.position"),
				light->position.x, light->position.y, light->position.z);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.ambient"),
				light->ambient.r, light->ambient.g, light->ambient.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.diffuse"),
				light->diffuse.r, light->diffuse.g, light->diffuse.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.specular"),
				light->specular.r, light->specular.g, light->specular.b);

		}
		
		errorEnum = glGetError();
		ALS_ASSERT(errorEnum == GL_NO_ERROR);
		
		glDrawArrays(GL_TRIANGLES, 0, scene->commonShapes.sphere.numTriangles * 3);
	}
	glBindVertexArray(0);

	errorEnum = glGetError();
	ALS_ASSERT(errorEnum == GL_NO_ERROR);
}

void drawCircle(Scene* scene, Vec3 position, Vec3 normal, real32 radius, Vec3 color)
{
	if(radius <= 0.0f)
	{
		return;
	}

	normal = normalize(normal);
	
	glUseProgram(basicShader);
	GLint currentShader;
	glGetIntegerv(GL_CURRENT_PROGRAM, &currentShader);
	
	glBindVertexArray(scene->commonShapes.circle.vao);
	{
		const Vec3 defaultNormal = { 0.0f, 1.0f, 0.0f };

		// THINK: is quaternions overkill for this?
		Quaternion q = rotateVector(defaultNormal, normal);

		Mat4 translate = translationMatrix(position);
		Mat4 rotate = rotationMatrix4(q);
		Mat4 scale = scalingMatrix(vector3(radius, radius, radius));
		
		Mat4 model = translate * rotate * scale;
		
		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "model"),
			1, GL_TRUE, model.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "view"),
			1, GL_TRUE, scene->view.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "projection"),
			1, GL_TRUE, scene->projection.dataPointer());

		// Material (use a basic default material)
		real32 aTerm = 1.0f;
		real32 dTerm = 1.0f;
		real32 sTerm = 1.0f;
		real32 shininess = 8.0f;
			
		glUniform3f(
			glGetUniformLocation(currentShader, "material.ambient"),
			aTerm * color.r, aTerm * color.g, aTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.diffuse"),
			dTerm * color.r, dTerm * color.g, dTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.specular"),
			sTerm * color.r, sTerm * color.g, sTerm * color.b);

		glUniform1f(
			glGetUniformLocation(currentShader, "material.shininess"),
			shininess);

		glUniform3f(
			glGetUniformLocation(currentShader, "viewPos"),
			scene->camera->position.x, scene->camera->position.y, scene->camera->position.z);

		for(int i = 0; i < scene->lightCount; i++)
		{
			Light* light = &scene->lights[i];
			
			// TODO: actually store several lights in the shader instead
			// of just overwriting the single light
			glUniform3f(
				glGetUniformLocation(currentShader, "light.position"),
				light->position.x, light->position.y, light->position.z);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.ambient"),
				light->ambient.r, light->ambient.g, light->ambient.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.diffuse"),
				light->diffuse.r, light->diffuse.g, light->diffuse.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.specular"),
				light->specular.r, light->specular.g, light->specular.b);
		}
		
		glDrawArrays(GL_TRIANGLES, 0, scene->commonShapes.circle.numTriangles * 3);
	}
	glBindVertexArray(0);
}
void drawRectPrism(Scene* scene, Vec3 position, Quaternion orientation, Vec3 scale, Vec3 color)
{
	if(scale.x <= 0.0f || scale.y <= 0.0f || scale.z <= 0.0f)
	{
		return;
	}

	ALS_ASSERT(isNormal(orientation));
	
	Mat4 translateM, scaleM, rotateM;
	
	glUseProgram(basicShader);
	GLint currentShader;
	glGetIntegerv(GL_CURRENT_PROGRAM, &currentShader);
	
	glBindVertexArray(scene->commonShapes.cube.vao);
	{
		translateM = translationMatrix(position);
		rotateM = rotationMatrix4(orientation);
		scaleM = scalingMatrix(scale);
		
		Mat4 model = translateM * rotateM * scaleM;
		
		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "model"),
			1, GL_TRUE, model.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "view"),
			1, GL_TRUE, scene->view.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "projection"),
			1, GL_TRUE, scene->projection.dataPointer());

		// Material (use a basic default material)
		real32 aTerm = 1.0f;
		real32 dTerm = 1.0f;
		real32 sTerm = 1.0f;
		real32 shininess = 8.0f;
			
		glUniform3f(
			glGetUniformLocation(currentShader, "material.ambient"),
			aTerm * color.r, aTerm * color.g, aTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.diffuse"),
			dTerm * color.r, dTerm * color.g, dTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.specular"),
			sTerm * color.r, sTerm * color.g, sTerm * color.b);

		glUniform1f(
			glGetUniformLocation(currentShader, "material.shininess"),
			shininess);

		glUniform3f(
			glGetUniformLocation(currentShader, "viewPos"),
			scene->camera->position.x, scene->camera->position.y, scene->camera->position.z);

		for(int i = 0; i < scene->lightCount; i++)
		{
			Light* light = &scene->lights[i];
			
			// TODO: actually store several lights in the shader instead
			// of just overwriting the single light
			glUniform3f(
				glGetUniformLocation(currentShader, "light.position"),
				light->position.x, light->position.y, light->position.z);

			// glUniform3f(
			// 	glGetUniformLocation(currentShader, "light.ambient"),
			// 	light->ambient.r, light->ambient.g, light->ambient.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.ambient"),
				0.5f, 0.5f, 0.5f);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.diffuse"),
				light->diffuse.r, light->diffuse.g, light->diffuse.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.specular"),
				light->specular.r, light->specular.g, light->specular.b);
		}
		
		glDrawArrays(GL_TRIANGLES, 0, scene->commonShapes.cylinder.numTriangles * 3);
	}
	glBindVertexArray(0);

	// DEBUG CODE: draws a sphere in the "forward" direction of the cylinder
	
	// Vec4 forward = vector4(0, 0, 1, 0);
	// forward = rotateM * forward;

	// Vec3 forward3D = vector3(forward.x, forward.y, forward.z);
	
	// drawSphere(scene,
	// 		   position + forward3D * scale.z * 1.1,
	// 		   .3,
	// 		   vector3(1, 1, 1)
	// 	);
}

void drawCube(Scene* scene, Vec3 position, Quaternion orientation, real32 sideLength, Vec3 color)
{
	drawRectPrism(scene, position, orientation, vector3(sideLength), color);
}

void drawCylinder(Scene* scene, Vec3 position, Quaternion orientation, real32 radius, real32 height, Vec3 color)
{
	if(radius <= 0.0f || height <= 0.0f)
	{
		return;
	}

	ALS_ASSERT(isNormal(orientation));

	Mat4 translate, scale, rotate;
	
	glUseProgram(basicShader);
	GLint currentShader;
	glGetIntegerv(GL_CURRENT_PROGRAM, &currentShader);
	
	glBindVertexArray(scene->commonShapes.cylinder.vao);
	{
		translate = translationMatrix(position);
		rotate = rotationMatrix4(orientation);
		scale = scalingMatrix(vector3(radius, height, radius));
		
		Mat4 model = translate * rotate * scale;
		
		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "model"),
			1, GL_TRUE, model.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "view"),
			1, GL_TRUE, scene->view.dataPointer());

		glUniformMatrix4fv(
			glGetUniformLocation(currentShader, "projection"),
			1, GL_TRUE, scene->projection.dataPointer());

		// Material (use a basic default material)
		real32 aTerm = 1.0f;
		real32 dTerm = 1.0f;
		real32 sTerm = 1.0f;
		real32 shininess = 8.0f;
			
		glUniform3f(
			glGetUniformLocation(currentShader, "material.ambient"),
			aTerm * color.r, aTerm * color.g, aTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.diffuse"),
			dTerm * color.r, dTerm * color.g, dTerm * color.b);

		glUniform3f(
			glGetUniformLocation(currentShader, "material.specular"),
			sTerm * color.r, sTerm * color.g, sTerm * color.b);

		glUniform1f(
			glGetUniformLocation(currentShader, "material.shininess"),
			shininess);

		glUniform3f(
			glGetUniformLocation(currentShader, "viewPos"),
			scene->camera->position.x, scene->camera->position.y, scene->camera->position.z);

		for(int i = 0; i < scene->lightCount; i++)
		{
			Light* light = &scene->lights[i];
			
			// TODO: actually store several lights in the shader instead
			// of just overwriting the single light
			glUniform3f(
				glGetUniformLocation(currentShader, "light.position"),
				light->position.x, light->position.y, light->position.z);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.ambient"),
				light->ambient.r, light->ambient.g, light->ambient.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.diffuse"),
				light->diffuse.r, light->diffuse.g, light->diffuse.b);

			glUniform3f(
				glGetUniformLocation(currentShader, "light.specular"),
				light->specular.r, light->specular.g, light->specular.b);
		}
		
		glDrawArrays(GL_TRIANGLES, 0, scene->commonShapes.cylinder.numTriangles * 3);
	}
	glBindVertexArray(0);

	// DEBUG CODE: draws a sphere in the "forward" direction of the cylinder
	
	// Vec4 forward = vector4(0, 0, 1, 0);
	// forward = rotate * forward;

	// Vec3 forward3D = vector3(forward.x, forward.y, forward.z);

	// drawSphere(scene,
	// 		   position + forward3D * radius * 1.1,
	// 		   .3,
	// 		   vector3(1, 1, 1)
	// 	);
}

void drawLine(Scene* scene, Vec3 start, Vec3 end, Vec4 color)
{
	glUseProgram(debugShader);
	GLint currentShader;
	glGetIntegerv(GL_CURRENT_PROGRAM, &currentShader);
	
	glUniformMatrix4fv(
		glGetUniformLocation(currentShader, "view"),
		1, GL_TRUE, scene->view.dataPointer());

	glUniformMatrix4fv(
		glGetUniformLocation(currentShader, "projection"),
		1, GL_TRUE, scene->projection.dataPointer());

	GLuint lineVBO;
	GLuint lineVAO;
	glGenBuffers(1, &lineVBO);
	glGenVertexArrays(1, &lineVAO);

	glBindVertexArray(lineVAO);
	{
		GLfloat vertices[14] =
			{
				// Position                      // Color
				start.x, start.y, start.z,       color.r, color.g, color.b, color.a,
				end.x, end.y, end.z,             color.r, color.g, color.b, color.a,
			};
		
		glBindBuffer(GL_ARRAY_BUFFER, lineVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
		
		
		glDrawArrays(GL_LINES, 0, 2);
	}
	glBindVertexArray(0);

	glDeleteBuffers(1, &lineVBO);
	glDeleteVertexArrays(1, &lineVAO);
}

void drawLine(Scene* scene, Vec3 start, Vec3 end, Vec3 color)
{
	drawLine(scene, start, end, vector4(color, 1));
}

void drawText(Scene* scene, Vec2 screenPos, Vec2 windowSize, char* str, Vec4 color)
{
	glUseProgram(textShader);
	GLint currentShader;
	glGetIntegerv(GL_CURRENT_PROGRAM, &currentShader);

	real32 x = screenPos.x;
	real32 y = screenPos.y;
		
	while(*str)
	{
		if(*str == '\n')
		{
			y += scene->font->lineHeight;
			x = screenPos.x;
		}
		
		GLuint textVAO;
		glGenVertexArrays(1, &textVAO);
		GLuint textVBO;
		glGenBuffers(1, &textVBO);
			
		glBindVertexArray(textVAO);
		{
			stbtt_aligned_quad q;
			stbtt_GetBakedQuad(
				scene->font->characterData,
				scene->font->glTextureDimension, scene->font->glTextureDimension,
				(*str) - 32,
				&x, &y,
				&q, 1
			);

			GLfloat vertices[] = {
				// Screen coordinate       // Tex coordinate
				
				// Triangle 1
				q.x0, q.y0,                q.s0, q.t0,
				q.x1, q.y0,                q.s1, q.t0,
				q.x1, q.y1,                q.s1, q.t1,

				// Triangle 2
				q.x1, q.y1,                q.s1, q.t1,
				q.x0, q.y1,                q.s0, q.t1,
				q.x0, q.y0,                q.s0, q.t0,

				// OPPOSITE WINDING -- I'm tired of having to switch while debugging stuff,
				// so lets just draw it both ways for now. This can be removed before shipping
				// (will also need to change the # of triangles in the glDrawArrays call
				
				// Triangle 1
				q.x0, q.y0,                q.s0, q.t0,
				q.x1, q.y1,                q.s1, q.t1,
				q.x1, q.y0,                q.s1, q.t0,

				// Triangle 2
				q.x1, q.y1,                q.s1, q.t1,
				q.x0, q.y0,                q.s0, q.t0,
				q.x0, q.y1,                q.s0, q.t1
			};


			glBindBuffer(GL_ARRAY_BUFFER, textVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
				
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
			glEnableVertexAttribArray(0);

			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(sizeof(GLfloat) * 2));
			glEnableVertexAttribArray(1);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, scene->font->glTexture);

			ALS_ASSERT(glGetUniformLocation(currentShader, "windowSize") != -1);
			ALS_ASSERT(glGetUniformLocation(currentShader, "fontTexture") != -1);
			
			glUniform2f(
				glGetUniformLocation(currentShader, "windowSize"),
				windowSize.x,
				windowSize.y
			);

			glUniform4f(
				glGetUniformLocation(currentShader, "targetColor"),
				color.r,
				color.g,
				color.b,
				color.a
			);

			glUniform1i(
				glGetUniformLocation(currentShader, "fontTexture"),
				0 // Texture 0!
			);
			
			glDrawArrays(GL_TRIANGLES, 0, ALS_ARRAY_LEN(vertices));

			GLenum errorEnum = glGetError();
			ALS_ASSERT(errorEnum == GL_NO_ERROR);
		}
		glBindVertexArray(0);
			
		glDeleteBuffers(1, &textVBO);
		glDeleteVertexArrays(1, &textVAO);

		GLenum errorEnum = glGetError();
		ALS_ASSERT(errorEnum == GL_NO_ERROR);
			
		str++;
	}
}

void drawWall(Scene* scene, Wall* wall)
{
	// Construct rectangle at default position, then rotate all the points at the end using orientation
	Vec3 wallForward = forwardVector(wall->orientation);
	Vec3 wallRight = rightVector(wall->orientation);
	
	Vec3 wallCeilCenter = wall->position + normalize(wall->position) * wall->height();

	Vec3 wallCeilTopLeft = wallCeilCenter - (wallRight * wall->xDim() / 2) + (wallForward * wall->yDim() / 2);
	Vec3 wallCeilBotLeft = wallCeilCenter - (wallRight * wall->xDim() / 2) - (wallForward * wall->yDim() / 2);

	Vec3 wallCeilTopRight = wallCeilCenter + (wallRight * wall->xDim() / 2) + (wallForward * wall->yDim() / 2);
	Vec3 wallCeilBotRight = wallCeilCenter + (wallRight * wall->xDim() / 2) - (wallForward * wall->yDim() / 2);

	Vec3 wallFloorTopLeft = wallCeilTopLeft - (normalize(wallCeilTopLeft) * wall->height());
	Vec3 wallFloorBotLeft = wallCeilBotLeft - (normalize(wallCeilBotLeft) * wall->height());

	Vec3 wallFloorTopRight = wallCeilTopRight - (normalize(wallCeilTopRight) * wall->height());
	Vec3 wallFloorBotRight = wallCeilBotRight - (normalize(wallCeilBotRight) * wall->height());

	// Floor
	drawQuad(scene, wallFloorTopLeft, wallFloorTopRight, wallFloorBotRight, wallFloorBotLeft, vector3(.8, .8, .8));

	// Front
	drawQuad(scene, wallCeilTopRight, wallFloorTopRight, wallFloorTopLeft, wallCeilTopLeft, vector3(.8, .8, .8));

	// Back
	drawQuad(scene, wallCeilBotLeft, wallFloorBotLeft, wallFloorBotRight, wallCeilBotRight, vector3(.8, .8, .8));

	// Left
	drawQuad(scene, wallCeilTopLeft, wallFloorTopLeft, wallFloorBotLeft, wallCeilBotLeft, vector3(.8, .8, .8));

	// Right
	drawQuad(scene, wallCeilTopRight, wallCeilBotRight, wallFloorBotRight, wallFloorTopRight, vector3(.8, .8, .8));

	// Top (todo: make curved)
	drawQuad(scene, wallCeilTopRight, wallCeilTopLeft, wallCeilBotLeft, wallCeilBotRight, vector3(.8, .8, .8));

	// DEBUG
	// draws an "arrow" in the forward facing direction
	{
		Vec3 wallFloorTopCenter = (wallFloorTopRight + wallFloorTopLeft) / 2.0;
		Vec3 arrowDelta = wallFloorTopCenter - wall->position; 
		drawTriangle(scene, wallFloorTopRight, wallFloorTopCenter + arrowDelta, wallFloorTopLeft, vector3(1, 0, 0));
	}
}

void drawArrow(Scene* scene, Vec3 start, Vec3 end, real32 endRadius, Vec4 color)
{
	drawLine(scene, start, end, color);
	// drawSphere(scene, end, endRadius, color); // TODO: support vector4 for all primitives

	drawSphere(scene, end, endRadius, vector3(color.r, color.g, color.b));
}

void drawArrow(Scene* scene, Vec3 start, Vec3 end, real32 endRadius, Vec3 color)
{
	drawArrow(scene, start, end, endRadius, vector4(color, 1));
}
