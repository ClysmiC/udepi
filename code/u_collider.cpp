enum ColliderType
{
	SPHERE,
	CYLINDER,
	RECT3,
	ROUND_PYRAMID
};

struct Entity;
struct SphereCollider
{
	real32 radius;
	
	Entity* attachedEntity;
};

SphereCollider* constructSphereCollider(real32 radius, Entity* entity)
{
	SphereCollider* collider;
	ALS_ALLOC(SphereCollider, collider, 1);
	collider->radius = radius;
	collider->attachedEntity = entity;

	return collider;
}

struct CylinderCollider
{
	real32 radius;
	real32 height;
	
	Entity* attachedEntity;
};

CylinderCollider* constructCylinderCollider(real32 radius, real32 height, Entity* entity)
{
	CylinderCollider* collider;
	ALS_ALLOC(CylinderCollider, collider, 1);
	collider->radius = radius;
	collider->height = height;
	collider->attachedEntity = entity;
	
	return collider;
}

struct BoxCollider
{
	Vec3 dimensions;
	
	Entity* attachedEntity;
};

BoxCollider* constructBoxCollider(Vec3 dimensions, Entity* entity)
{
	BoxCollider* collider;
	ALS_ALLOC(BoxCollider, collider, 1);
	collider->dimensions = dimensions;
	collider->attachedEntity = entity;

	return collider;
}

// http://imgur.com/m90WRDe
struct WallCollider
{
	Vec2 dimensions;
	real32 height;

	Entity* attachedEntity;
};

WallCollider* constructWallCollider(Vec2 dimensions, real32 height, Entity* entity)
{
	WallCollider* collider;
	ALS_ALLOC(WallCollider, collider, 1);
	collider->dimensions = dimensions;
	collider->height = height;
	collider->attachedEntity = entity;

	return collider;
}
