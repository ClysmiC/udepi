Vec3 gjkSupport(Entity* entity, Vec3 direction);

union RelativeVector
{
	struct
	{
		Vec3 asVector;
	};

	struct
	{
		real32 right;
		real32 forward;
		real32 up;
	};

	struct
	{
		Vec2 asVector2;
		real32 _ignored0_;
	};
};

enum EntityFlags
{
	DYNAMIC_MOVE               = (0x1 << 0),
	POSITION_AT_BOTTOM         = (0x1 << 1) 
};

enum EntityType
{
	UDEPI,
	LEVEL,
	WALL
};

struct Entity
{
	Vec3 position;
	Quaternion orientation;

	ColliderType colliderType;
	void* collider;
	
	uint32 flags;

	// Returns the point on the entity's collider that intersects with
	// a ray cast from the entity center projected downwards
	// (determined by orientation)
	Vec3 bottomPoint()
	{
		Vec3 result;

		if(flags & POSITION_AT_BOTTOM)
		{
			result = position;
		}
		else
		{
			Vec3 down = -upVector(orientation);
			result = gjkSupport(this, down);
		}

		return result;
	}

	Vec3 centerPoint()
	{
		Vec3 result = position;
		
		if(flags & POSITION_AT_BOTTOM)
		{
			Vec3 up = upVector(orientation);
			
			switch(colliderType)
			{
				case SPHERE:
				{
					SphereCollider* collider = (SphereCollider*)this->collider;
					result += up * collider->radius;
				} break;

				case CYLINDER:
				{
					CylinderCollider* collider = (CylinderCollider*)this->collider;
					result += up * collider->height / 2;
				} break;

				case RECT3:
				{
					BoxCollider* collider = (BoxCollider*)this->collider;
					result += up * collider->dimensions.z / 2;
				} break;

				default:
				{
					ALS_ILLEGAL;
				}
			}
		}

		return result;
	}

	Plane getStandingPlane()
	{
		Plane result;

		result.point = bottomPoint();
		result.normal = normalize(position);

		return result;
	}

	void freeMe()
	{
		if(collider)
		{
			free(collider);
		}
		
		free(this);
	}
};
