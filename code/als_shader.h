#ifndef ALS_SHADER_H
#define ALS_SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

class Shader
{
public:
	GLuint id;
	
	Shader(const GLchar *vPath, const GLchar *fPath)
	{
		std::string vSrc;
		std::string fSrc;
		std::ifstream vFile;
		std::ifstream fFile;

		vFile.exceptions(std::ifstream::badbit);
		fFile.exceptions(std::ifstream::badbit);

		try
		{
			vFile.open(vPath);
			fFile.open(fPath);
			std::stringstream vStream, fStream;

			vStream << vFile.rdbuf();
			fStream << fFile.rdbuf();

			vFile.close();
			fFile.close();

			vSrc = vStream.str();
			fSrc = fStream.str();
		}
		catch(std::ifstream::failure e)
		{
			printf("ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ\n");
		}

		const GLchar *vCode = vSrc.c_str();
		const GLchar *fCode = fSrc.c_str();

		GLuint vHandle, fHandle;
		GLint success;

		vHandle = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vHandle, 1, &vCode, NULL);
		glCompileShader(vHandle);

		glGetShaderiv(vHandle, GL_COMPILE_STATUS, &success);

		if(!success)
		{
			GLchar error[512];
			glGetShaderInfoLog(vHandle, 512, NULL, error);
			printf("ERROR::SHADER::VERTEX::COMPLIATION_FAILED\n%s", error);
		}

		fHandle = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fHandle, 1, &fCode, NULL);
		glCompileShader(fHandle);

		glGetShaderiv(fHandle, GL_COMPILE_STATUS, &success);

		if(!success)
		{
			GLchar error[512];
			glGetShaderInfoLog(fHandle, 512, NULL, error);
			printf("ERROR::SHADER::FRAGMENT::COMPLIATION_FAILED\n%s", error);
		}

		id = glCreateProgram();
		glAttachShader(id, vHandle);
		glAttachShader(id, fHandle);
		glLinkProgram(id);

		glGetProgramiv(id, GL_LINK_STATUS, &success);

		if(!success)
		{
			GLchar error[512];
			glGetProgramInfoLog(id, 512, NULL, error);
			printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s", error);
		}

		glDeleteShader(vHandle);
		glDeleteShader(fHandle);
	}
	
	void use()
	{
		glUseProgram(id);
	}
};

#endif
