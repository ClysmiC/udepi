struct Camera
{
	Vec3 position;
	Vec3 target = { 0, 0, 0 };
	Quaternion orientation;

	float fov; // Note: Degrees
	float aspectRatio; // does this belong in camera??

	// culling distances
	float near;
	float far;

	
	// FUNCTIONS
	
	Vec3 forward()
	{
		Vec3 result = forwardVector(orientation);

		ALS_ASSERT(equals(result, normalize(target - position)));
		return result;
	}

	Vec3 up()
	{
		Vec3 result = upVector(orientation);
		return result;
	}

	Vec3 right()
	{
		Vec3 result = rightVector(orientation);
		return result;
	}

	Mat4 projectionMatrix()
	{
		// http://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix/building-basic-perspective-projection-matrix_transform
		Mat4 result = {};

		ALS_ASSERT(this->fov > 0 && this->aspectRatio > 0);
		ALS_ASSERT(far > near);

		result[1][1] = 1 / tan(TO_RAD(fov/2));
		result[0][0] = result[1][1] / aspectRatio;

		result[2][2] = -far / (far - near);
		result[2][3] = -(far * near) / (far - near);

		result[3][2] = -1;
		result[3][3] = 0;

		return result;
	}

	Mat4 viewMatrix()
	{
		// http://learnopengl.com/#!Getting-started/Camera
		Mat4 result;
		
		// Invert (transpose) the ortho-normal rotation matrix
		// i.e., rotate *everything* by the opposite of the camera's
		// rotation.
		Vec3 up = this->up();
		Vec3 right = this->right();
		Vec3 forward = this->forward();
		
		Mat4 rotation = {};

		// NOTE: This matrix wants the vector pointing TOWARDS the camera
		// i.e., -forward
		
		rotation[0][0] = right.x;
		rotation[1][0] = up.x;
		rotation[2][0] = -forward.x;

		rotation[0][1] = right.y;
		rotation[1][1] = up.y;
		rotation[2][1] = -forward.y;
		
		rotation[0][2] = right.z;
		rotation[1][2] = up.z;
		rotation[2][2] = -forward.z;

		rotation[3][3] = 1;

		// Translate the result by the opposite of the camera's position
		// i.e., transalte *everything* by the opposite of the camera's
		// position.
		Mat4 translation = translationMatrix(-this->position);

		result = rotation * translation;
		return result;
	}

	bool32 isNorthUp()
	{
		bool32 result;
		result = this->up().y >= 0;

		return result;
	}

	// Poorly named ?
	// This is useful for using as a coefficient in equations that require
	// one or more vectors to be flipped if the camera is south up.
	int32 flipTest()
	{
		if(this->isNorthUp())
		{
			return 1;
		}

		return -1;
	}
};

Mat4 debug_orthographicMatrix(real32 width, real32 height, real32 near, real32 far)
{
	real32 halfW = width / 2;
	real32 halfH = height / 2;

	real32 points[4][4] = {
		{ 1 / halfW, 0, 0, 0 },
		{ 1 / halfH, 0, 0, 0 },
		{ 0, 0, },
		{ 0, 0, 0, 1}
	};
	
	Mat4 result;
	result[0][0] = 1 / halfW;
	result[0][1] = 0;
	result[0][2] = 0;
	result[0][3] = 0;

	result[1][0] = 0;
	result[1][1] = 1 / halfH;
	result[1][2] = 0;
	result[1][3] = 0;

	result[2][0] = 0;
	result[2][1] = 0;
	result[2][2] = -2 / (far - near);
	result[2][3] = -(far + near) / (far - near);

	result[3][0] = 0;
	result[3][1] = 0;
	result[3][2] = 0;
	result[3][3] = 1;

	return result;
}
